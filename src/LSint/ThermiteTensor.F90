!> Module for internal integral and matrix storage and operations
MODULE ThermiteTensor
use precision
use typeDefType, only: lssetting
use ls_inputAO_mod
use ao_typetype, only: aoitem
use memory_handling, only: mem_alloc, mem_dealloc
use lsparameters
use thermiteTensor_type

public :: init_tt,free_tt,print_tt,tt_assign_elms,tt_copy_elms

INTERFACE init_tt
   MODULE PROCEDURE init_tt5,init_ttn
END INTERFACE init_tt

INTERFACE tt_copy_elms
   MODULE PROCEDURE tt_copy_elms_full,tt_copy_elms_subblock
END INTERFACE tt_copy_elms

PRIVATE
CONTAINS 

!> Initialize the Thermite tensor object
!> \param tt Thermite tensor
!> \param AO Specify the type of AOs
!> \param indAO Specify the index of each AO
!> \param nAO The number of AOs
!> \param dim5 Joint dimension for integral components/#of contractions/matrices
!> \param ndim5 Number of such joint dimensions
!> \param intType Contracted or primitive
!> \param Setting Integral setting with molecule and basis set info
!> \param lupri Default print unit
!> \param luerr Error print unit
SUBROUTINE init_ttn(tt,AO,indAO,nAO,dim5,ndim5,intType,Setting,lupri,luerr)
implicit none
Type(TT_type),intent(INOUT)   :: tt
Type(LSSETTING),intent(INOUT) :: Setting
Integer, intent(IN)           :: AO(nAO),indAO(nAO),nAO,dim5(ndim5),ndim5,intType,lupri,luerr
!
Integer :: AOtype(4),iAO,order(4+ndim5),emptyAO(4),i

IF (nAO.GT.4) THEN
  write(*,*) 'Programming error. init_ttn called with nAO =',nAO
  call lsquit('Programming error. init_ttn called with nAO > 4',-1)
ENDIF

DO iAO=1,nAO
  IF ((indAO(iAO).LE.0).OR.(indAO(iAO).GT.4)) THEN
    write(*,*) 'Programming error. init_ttn called with indAO =',indAO(iAO),' for iAO =',iAO
    call lsquit('Programming error. init_ttn called with indAO outside the interval 1-4',-1)
  ENDIF
ENDDO

!Set up the AOs according to indAO, emptying the missing ones
AOtype(1:4) = AOempty
DO iAO=1,nAO
  AOtype(indAO(iAO)) = AO(iAO)
ENDDO

!Store the missing ones
i=0
DO iAO=1,4
  IF (AOtype(iAO).EQ.AOempty) THEN
    i=i+1
    emptyAO(i) = iAO
  ENDIF
ENDDO

IF (nAO+i.NE.4) call lsquit('Programming error. Something wrong with indeces in init_ttn',-1)

call init_tt5(tt,AOtype(1),AOtype(2),AOtype(3),AOtype(4),dim5,ndim5,intType,Setting,lupri,luerr)

!Reorder tt according to indAO
order(1:nAO)   = indAO(:)
DO i=1,ndim5
  order(nAO+i) = 4+i
ENDDO
order(nAO+ndim5+1:4+ndim5) = emptyAO(1:4-nAO)
call tt_reorder(tt,order,4+ndim5)

!Remove the empty AOs
call  tt_remove_AOinfo(tt,nAO+ndim5)

END SUBROUTINE init_ttn

!
!> Initialize the 5-dim Thermite tensor object
!> \param tt Thermite tensor
!> \param AO1 Specify the type of AO number 1
!> \param AO2 Specify the type of AO number 2
!> \param AO3 Specify the type of AO number 3
!> \param AO4 Specify the type of AO number 4
!> \param dim5 Joint dimension for integral components/#of contractions/matrices
!> \param ndim5 Number of such joint dimensions
!> \param intType Contracted or primitive
!> \param Setting Integral setting with molecule and basis set info
!> \param lupri Default print unit
!> \param luerr Error print unit
SUBROUTINE init_tt5(tt,AO1,AO2,AO3,AO4,dim5,ndim5,intType,Setting,lupri,luerr)
implicit none
Type(TT_type),intent(INOUT)   :: tt
Type(LSSETTING),intent(INOUT) :: Setting
Integer, intent(IN)           :: AO1,AO2,AO3,AO4,dim5(ndim5),ndim5,intType,lupri,luerr
!
Logical              :: CSint,LHSGAB,sameLHSaos,sameRHSaos,sameODs
TYPE(AOITEM),target  :: AObuild(4)
Integer              :: nAObuilds,indAO(4),ndim(4)
Integer              :: natoms,nbatches,iAO,iBuild,nelms,iAtom,nOrb,iAngmom,iBatch,start,AOtype,idim5
Integer              :: nAngmom_max,nAngmom
Integer,pointer      :: atom_num_orb(:),atom_start_orb(:),batch_num_orb(:,:)
Integer,pointer      :: batch_start_orb(:,:),batch_atom(:),batch_nAngmom(:),one1(:),one2(:,:)

tt%ndim = 4+ndim5
allocate(tt%AO_info(tt%ndim))
DO iAO=1,tt%ndim
  allocate(tt%AO_info(iAO)%p)
ENDDO
tt%elms_status = elms_status_unassigned
nullify(tt%elms)

CSint  = .FALSE.
LHSGAB = .FALSE.
!Build orbital information
call BuildInputAO(AO1,AO2,AO3,AO4,intType,AObuild,nAObuilds,indAO,ndim,CSint,LHSGAB,&
    &             sameLHSaos,sameRHSaos,sameODs,SETTING,LUPRI,LUERR)

!Calculate the number of elements
nelms=1
DO iAO=1,4
  nelms=nelms*ndim(indAO(iAO))
ENDDO
DO idim5=1,ndim5
  nelms=nelms*dim5(idim5)
ENDDO
tt%nelms = nelms

DO iBuild=1,nAObuilds

  !Set up mapping for each AO_build
  natoms   = AObuild(iBuild)%natoms
  nbatches = AObuild(iBuild)%nbatches

  !Find the maximum number of angular shells per batch for given build
  nAngmom_max = 0
  DO iBatch=1,nbatches
    nAngmom_max = max(nAngmom_max,AObuild(iBuild)%BATCH(iBatch)%nAngmom)
  ENDDO

  call mem_alloc(atom_num_orb,natoms)
  call mem_alloc(atom_start_orb,natoms)
  call mem_alloc(batch_num_orb,nbatches,nAngmom_max)
  call mem_alloc(batch_start_orb,nbatches,nAngmom_max)
  call mem_alloc(batch_nAngmom,nbatches)
  call mem_alloc(batch_atom,nbatches)
  call ls_izero(atom_num_orb,natoms)
  call ls_izero(atom_start_orb,natoms)
  call ls_izero(batch_num_orb,nbatches*nAngmom_max)
  call ls_izero(batch_start_orb,nbatches*nAngmom_max)
  call ls_izero(batch_nAngmom,nbatches)
  call ls_izero(batch_atom,nbatches)
  DO iBatch=1,nbatches
    iAtom   = AObuild(iBuild)%BATCH(iBatch)%atom
    nAngmom = AObuild(iBuild)%BATCH(iBatch)%nAngmom
    batch_atom(iBatch)    = iAtom
    batch_nAngmom(iBatch) = nAngmom
    DO iAngmom=1,nAngmom
      nOrb   = AObuild(iBuild)%BATCH(iBatch)%nOrbitals(iAngmom)
      start  = AObuild(iBuild)%BATCH(iBatch)%startOrbital(iAngmom)
      batch_num_orb(iBatch,iAngmom)   = nOrb
      batch_start_orb(iBatch,iAngmom) = start
      atom_num_orb(iAtom)             = atom_num_orb(iAtom) + nOrb
      IF(atom_start_orb(iAtom).EQ.0) atom_start_orb(iAtom) = start
    ENDDO
  ENDDO

  !Allocate and build tt mapping
  DO iAO=1,4
    IF (indAO(iAO) .EQ. iBuild) THEN
      AOtype = AOtype_AO
      IF (AObuild(iBuild)%empty) THEN
        call mem_alloc(one1,1)
        call mem_alloc(one2,1,1)
        one1(1) = 1
        one2(1,1) = 1
        call tt_map_alloc(tt%ao_info(iAO)%p,1,1,1,1,AOtype_empty)
        call tt_AOmap_set(tt%ao_info(iAO)%p,one1,one1,one2,one2,one1,one1)
        call mem_dealloc(one1)
        call mem_dealloc(one2)
      ELSE
        call tt_map_alloc(tt%ao_info(iAO)%p,natoms,nbatches,ndim(iBuild),nAngmom_max,AOtype_AO)
        call tt_AOmap_set(tt%ao_info(iAO)%p,atom_num_orb,atom_start_orb,batch_num_orb,&
     &                    batch_start_orb,batch_atom,batch_nAngmom)
      ENDIF
    ENDIF
  ENDDO
  call mem_dealloc(atom_num_orb)
  call mem_dealloc(atom_start_orb)
  call mem_dealloc(batch_num_orb)
  call mem_dealloc(batch_start_orb)
  call mem_dealloc(batch_nAngmom)
  call mem_dealloc(batch_atom)
ENDDO
DO idim5=1,ndim5
  call tt_map_alloc(tt%ao_info(4+idim5)%p,1,1,dim5(idim5),1,AOtype_dim5)
ENDDO
CALL FreeInputAO(AObuild,nAObuilds,lupri)
END SUBROUTINE init_tt5

!> Reorders AO_info
!> \param tt Thermite tensor
!> \param order The order the AOs should appear in
!> \param ndim The dimensionality of the tt tensor
SUBROUTINE tt_reorder(tt,order,ndim)
implicit none
Type(TT_type),intent(INOUT)   :: tt
integer,intent(IN)            :: order(ndim),ndim
!
Type(tt_ao_info_pt) :: aoinfo_orig(ndim)
Integer             :: i

!Consistency tests
IF (tt%ndim.NE.ndim) call lsquit('Programming error. tt_reorder ndim mismatch',-1)

DO i=1,ndim
  IF ((order(i).LE.0).OR.(order(i).GT.ndim)) THEN
    write(*,*) 'Programming error. tt_reorder call with order outside 1-ndim: order=',order(i),' i=',i
    call lsquit('Programming error. tt_reorder call with order outside 1-ndim',-1)
  ENDIF
ENDDO

!Store original order
DO i=1,ndim
  aoinfo_orig(i)%p => tt%ao_info(i)%p
ENDDO

!Set up new order according to order
DO i=1,ndim
  tt%ao_info(i)%p => aoinfo_orig(order(i))%p
ENDDO

END SUBROUTINE tt_reorder


!> Removes AO_info information and reduce dimensionality
!> \param tt Thermite tensor
!> \param keep Indicate which dimension to keep
SUBROUTINE tt_remove_AOinfo(tt,keep)
implicit none
Type(TT_type),intent(INOUT)   :: tt
integer,intent(IN)            :: keep
!
Integer :: idim
!Consistency tests
IF (tt%ndim.LT.keep) call lsquit('Programming error. tt_remove_AOinfo keep > ndim',-1)

DO idim=keep+1,tt%ndim
  IF (tt%ao_info(idim)%p%norb.NE.1) call lsquit('Programming error. Wrong use of tt_remove_AOinfo',-1)
  call tt_map_dealloc(tt%ao_info(idim)%p)
  deallocate(tt%ao_info(idim)%p)
ENDDO

tt%ndim = keep

END SUBROUTINE tt_remove_AOinfo

!> Frees the Thermite tensor object
!> \param tt Thermite tensor
SUBROUTINE free_tt(tt)
implicit none
Type(TT_type),intent(INOUT)   :: tt
!
Integer :: iAO

IF (tt%elms_status .EQ. elms_status_allocated) call mem_dealloc(tt%elms)
DO iAO=1,tt%ndim
  call tt_map_dealloc(tt%ao_info(iAO)%p)
  deallocate(tt%ao_info(iAO)%p)
ENDDO
deallocate(tt%AO_info)

END SUBROUTINE free_tt

!> Allocates the tt orbital mapping for one AO
!> \param ao_info The AO_info
!> \param natoms The number of atoms
!> \param nbatches The number of batches
!> \param norb The number of orbitals
!> \param nAngmom_max The maximal number of angular momentum shells per batch
!> \param AOtype The type of AO (from parameter list AOtype_XXX)
SUBROUTINE tt_map_alloc(ao_info,natoms,nbatches,norb,nAngmom_max,AOtype)
implicit none
Type(tt_ao_info),intent(INOUT) :: ao_info
Integer,intent(IN)             :: natoms,nbatches,norb,nAngmom_max,AOtype
AO_info%AOtype       = AOtype
AO_info%patype       = patype_full
IF (AOtype.EQ.AOtype_dim5) AO_info%patype = patype_none
AO_info%norb         = norb
AO_info%natoms       = natoms
AO_info%nbatches     = nbatches
AO_info%nAngmom_max  = nAngmom_max
call mem_alloc(AO_info%atom_num_orb,natoms)
call mem_alloc(AO_info%atom_start_orb,natoms)
call mem_alloc(AO_info%batch_num_orb,nbatches,nAngmom_max)
call mem_alloc(AO_info%batch_start_orb,nbatches,nAngmom_maX)
call mem_alloc(AO_info%batch_nAngmom,nbatches)
call mem_alloc(AO_info%batch_atom,nbatches)
END SUBROUTINE tt_map_alloc

!> Deallocates the tt orbital mapping for one AO
!> \papam ao_info The AO_info
SUBROUTINE tt_map_dealloc(ao_info)
implicit none
Type(tt_ao_info),intent(INOUT) :: ao_info
AO_info%AOtype    = 0
AO_info%patype    = 0
AO_info%norb      = 0
AO_info%natoms    = 0
AO_info%nbatches  = 0
call mem_dealloc(AO_info%atom_num_orb)
call mem_dealloc(AO_info%atom_start_orb)
call mem_dealloc(AO_info%batch_num_orb)
call mem_dealloc(AO_info%batch_start_orb)
call mem_dealloc(AO_info%batch_nAngmom)
call mem_dealloc(AO_info%batch_atom)
END SUBROUTINE tt_map_dealloc

!> Sets the tt orbital mapping for one AO
!> \papam ao_info The AO_info
!> \papam atom_num_orb The number of orbitals per atom
!> \papam atom_start_orb The starting orbital index for each atom
!> \papam batch_num_orb The number of orbital per batch
!> \papam batch_start_orb The starting orbital index for each batch
!> \papam batch_nAngmom The number of angular momenta for each batch
!> \papam batch_atom The atom index for each batch
SUBROUTINE tt_AOmap_set(ao_info,atom_num_orb,atom_start_orb,batch_num_orb,batch_start_orb,&
     &                  batch_atom,batch_nAngmom)
implicit none
Type(tt_ao_info),intent(INOUT) :: ao_info
Integer,pointer                :: atom_num_orb(:),atom_start_orb(:),batch_num_orb(:,:)
Integer,pointer                :: batch_start_orb(:,:),batch_atom(:),batch_nAngmom(:)
ao_info%atom_num_orb(:)      = atom_num_orb(:)
ao_info%atom_start_orb(:)    = atom_start_orb(:)
ao_info%batch_num_orb(:,:)   = batch_num_orb(:,:)
ao_info%batch_start_orb(:,:) = batch_start_orb(:,:)
ao_info%batch_nAngmom(:)     = batch_nAngmom(:)
ao_info%batch_atom(:)        = batch_atom(:)
END SUBROUTINE tt_AOmap_set

!> Prints the thermite tensor
!> \param tt Thermite tensor
!> \param iunit The print unit to print to (for example lupri pointing to LSDALTON.OUT)
!> \param print_elms Specifies if the tensor elements should be printed
SUBROUTINE print_tt(tt,iunit,print_elms)
implicit none
type string_array
Character (len=9),dimension(max_AOdims) :: string
end type string_array

Type(TT_type),intent(IN) :: tt
Integer,intent(IN)       :: iunit
Logical,intent(IN)       :: print_elms
!
Integer      :: i,j,ndim_max,max_atoms,max_batches,iAngmom,nAngmom_max
Character (len=9),dimension(tt%ndim) :: string
type(string_array),allocatable :: array(:)

ndim_max = min(tt%ndim,max_AOdims)

Write(iunit,'(A)') '*** Thermite Tensor output ***'
if (tt%ndim.GT.max_AOdims) write(iunit,'(A,I9,A,I2,A)') 'Printout warning, ndim =',tt%ndim,&
    &                          '. Printing only first ',max_AOdims,' AO_infos'

!AO
Write(iunit,'(A20,5X,10I9)') 'AO index         ',(i,i=1,ndim_max)
!AO type
nAngmom_max = 0
DO i=1,ndim_max
  nAngmom_max = max(nAngmom_max,tt%AO_info(i)%p%nAngmom_max)
  SELECT CASE(tt%AO_info(i)%p%AOtype)
  CASE(AOtype_Empty); string(i)='    Empty'
  CASE(AOtype_AO);    string(i)='       AO'
  CASE(AOtype_dim5);  string(i)='     dim5'
  CASE DEFAULT
    WRITE(iunit) '  n.a.'
  END SELECT
ENDDO
Write(iunit,'(A20,5X,10A9)') 'AO types         ',(string(i),i=1,ndim_max)

!Partitioning type
DO i=1,ndim_max
  SELECT CASE(tt%AO_info(i)%p%patype)
  CASE(patype_full);             string(i)='     Full'
  CASE(patype_atom_partition);   string(i)='     Atom'
  CASE(patype_batch_partition);  string(i)='    Batch'
  CASE(patype_none);             string(i)='     None'
  CASE DEFAULT
    WRITE(iunit) '  n.a.'
  END SELECT
ENDDO
Write(iunit,'(A20,5X,10A9)') 'Partitioning type',(string(i),i=1,ndim_max)

Write(iunit,'(A20,5X,10I9)') '# of atoms       ',(tt%AO_info(i)%p%nAtoms,i=1,ndim_max)
Write(iunit,'(A20,5X,10I9)') '# of batches     ',(tt%AO_info(i)%p%nBatches,i=1,ndim_max)
Write(iunit,'(A20,5X,10I9)') '# of orbitals    ',(tt%AO_info(i)%p%nOrb,i=1,ndim_max)
Write(iunit,'(A20,5X,10I9)') 'Max # of angmoms ',(tt%AO_info(i)%p%nAngmom_max,i=1,ndim_max)

max_atoms=0
max_batches=0
DO i=1,ndim_max
  max_atoms=max(max_atoms,tt%AO_info(i)%p%nAtoms)
  max_batches=max(max_batches,tt%AO_info(i)%p%nBatches)
ENDDO
allocate(array(max_atoms))

!Number of orbitals per atom
DO i=1,ndim_max
  DO j=1,tt%AO_info(i)%p%nAtoms
    write(array(j)%string(i),'(I9)') tt%AO_info(i)%p%atom_num_orb(j)
  ENDDO
  DO j=tt%AO_info(i)%p%nAtoms+1,max_atoms
     write(array(j)%string(i),'(9X)')
  ENDDO
ENDDO
write(iunit,'(1X)')  !Blank Line
write(iunit,'(A40)') '* Number of orbitals per atom *         '
write(iunit,'(A25,10I9)') '   Atom                AO',(i,i=1,ndim_max)
DO j=1,max_atoms
  Write(iunit,'(I5,20X,10A9)') j,(array(j)%string(i),i=1,ndim_max)
ENDDO

!Starting orbital for each atom
DO i=1,ndim_max
  DO j=1,tt%AO_info(i)%p%nAtoms
    write(array(j)%string(i),'(I9)') tt%AO_info(i)%p%atom_start_orb(j)
  ENDDO
  DO j=tt%AO_info(i)%p%nAtoms+1,max_atoms
     write(array(j)%string(i),'(9X)')
  ENDDO
ENDDO
write(iunit,'(1X)')  !Blank Line
write(iunit,'(A40)') '* Starting orbital for each atom *      '
write(iunit,'(A25,10I9)') '   Atom                AO',(i,i=1,ndim_max)
DO j=1,max_atoms
  Write(iunit,'(I5,20X,10A9)') j,(array(j)%string(i),i=1,ndim_max)
ENDDO

deallocate(array)
allocate(array(max_batches))

!Number of orbitals per batch
DO iAngmom=1,nAngmom_max
  DO i=1,ndim_max
    DO j=1,tt%AO_info(i)%p%nBatches
      IF (tt%AO_info(i)%p%batch_nAngmom(j).GE.iAngmom) THEN
        write(array(j)%string(i),'(I9)') tt%AO_info(i)%p%batch_num_orb(j,iAngmom)
      ELSE
         write(array(j)%string(i),'(9X)')
      ENDIF
    ENDDO
    DO j=tt%AO_info(i)%p%nBatches+1,max_batches
       write(array(j)%string(i),'(9X)')
    ENDDO
  ENDDO
  write(iunit,'(1X)')  !Blank Line
  write(iunit,'(A44,I3)') '* Number of orbitals per batch for iAngmom =',iAngmom
  write(iunit,'(A25,10I9)') '   Batch               AO',(i,i=1,ndim_max)
  DO j=1,max_batches
    Write(iunit,'(I5,20X,10A9)') j,(array(j)%string(i),i=1,ndim_max)
  ENDDO
ENDDO

!Starting orbital for each batch
DO iAngmom=1,nAngmom_max
  DO i=1,ndim_max
    DO j=1,tt%AO_info(i)%p%nBatches
      IF (tt%AO_info(i)%p%batch_nAngmom(j).GE.iAngmom) THEN
        write(array(j)%string(i),'(I9)') tt%AO_info(i)%p%batch_start_orb(j,iAngmom)
      ELSE
         write(array(j)%string(i),'(9X)')
      ENDIF
    ENDDO
    DO j=tt%AO_info(i)%p%nBatches+1,max_batches
       write(array(j)%string(i),'(9X)')
    ENDDO
  ENDDO
  write(iunit,'(1X)')  !Blank Line
  write(iunit,'(A47,I3)') '* Starting orbital for each batch for iAngmom =',iAngmom
  write(iunit,'(A25,10I9)') '   Batch               AO',(i,i=1,ndim_max)
  DO j=1,max_batches
    Write(iunit,'(I5,20X,10A9)') j,(array(j)%string(i),i=1,ndim_max)
  ENDDO
ENDDO

!Atomic index for each batch
DO i=1,ndim_max
  DO j=1,tt%AO_info(i)%p%nBatches
    write(array(j)%string(i),'(I9)') tt%AO_info(i)%p%batch_atom(j)
  ENDDO
  DO j=tt%AO_info(i)%p%nBatches+1,max_batches
     write(array(j)%string(i),'(9X)')
  ENDDO
ENDDO
write(iunit,'(1X)')  !Blank Line
write(iunit,'(A40)') '* Map between batch and atom *          '
write(iunit,'(A25,10I9)') '   Batch               AO',(i,i=1,ndim_max)
DO j=1,max_batches
  Write(iunit,'(I5,20X,10A9)') j,(array(j)%string(i),i=1,ndim_max)
ENDDO

!Number of angular momenta shells per batch
DO i=1,ndim_max
  DO j=1,tt%AO_info(i)%p%nBatches
    write(array(j)%string(i),'(I9)') tt%AO_info(i)%p%batch_nAngmom(j)
  ENDDO
  DO j=tt%AO_info(i)%p%nBatches+1,max_batches
     write(array(j)%string(i),'(9X)')
  ENDDO
ENDDO
write(iunit,'(1X)')  !Blank Line
write(iunit,'(A46)') '* Number of angular momenta shells per batch *'
write(iunit,'(A25,10I9)') '   Batch               AO',(i,i=1,ndim_max)
DO j=1,max_batches
  Write(iunit,'(I5,20X,10A9)') j,(array(j)%string(i),i=1,ndim_max)
ENDDO

write(iunit,'(1X)')  !Blank Line
SELECT CASE(tt%elms_status)
CASE(elms_status_unassigned)
  write(iunit,'(A,I2)') 'The tensor elements are unassigned'
CASE(elms_status_assigned)
  write(iunit,'(A,I2)') 'The tensor elements have been assigned (to externally allocated memory)'
  if (print_elms) THEN
    write(iunit,'(1X)')  !Blank Line
    call tt_elms_print(tt,iunit)
  endif
CASE(elms_status_allocated)
  write(iunit,'(A,I2)') 'The tensor elements have been allocated'
  if (print_elms) THEN
    write(iunit,'(1X)')  !Blank Line
    call tt_elms_print(tt,iunit)
  endif
CASE DEFAULT
  write(iunit,'(A,I2)') 'Error in tt: elms_status incorrectly set to ',tt%elms_status
END SELECT
write(iunit,'(A30)')  '******************************'
write(iunit,'(1X)')  !Blank Line

END SUBROUTINE print_tt

!> Prints the thermite tensor elements
!> \param tt Thermite tensor
!> \param iunit The print unit to print to
SUBROUTINE tt_elms_print(tt,iunit)
implicit none
Type(TT_type),intent(IN) :: tt
Integer,intent(IN)       :: iunit
!
Integer :: I(tt%ndim),N(tt%ndim)

call tt_elms_print_loop(tt,iunit,I,N,tt%ndim,tt%ndim)

END SUBROUTINE tt_elms_print

!> Loop and priting of the thermite tensor elements
!> \param tt Thermite tensor
!> \param iunit The print unit to print to
!> \param I Loop indeces
!> \param N Dimension sizes
!> \param ndim The dimension of the tensor
!> \param idim The current tensor index to loop over (decremental)
RECURSIVE SUBROUTINE tt_elms_print_loop(tt,iunit,I,N,ndim,idim)
implicit none
Type(TT_type),intent(IN) :: tt
Integer,intent(IN)       :: iunit,ndim,idim
Integer,intent(inout)    :: I(ndim),N(ndim)
!
Integer :: k,start,mult
N(idim) = tt%AO_info(idim)%p%nOrb
IF (idim.EQ.1) THEN
  I(1) = N(1)
  start = 0
  mult = 1
  DO k=2,ndim
    mult  = mult*N(k-1)
    start = start + (I(k)-1)*mult
  ENDDO
  write(iunit,'(A6,4X,10I9)') 'Index 1:',(I(k),k=1,ndim)
  write(iunit,'(10X,10F18.6)') (tt%elms(start+k),k=1,N(1))
ELSE
  DO k=1,N(idim)
    I(idim) = k
    call tt_elms_print_loop(tt,iunit,I,N,ndim,idim-1)
  ENDDO
ENDIF
END SUBROUTINE tt_elms_print_loop

!> Assigns the Thermite tensor elements (to point to memory allocated elsewhere)
!> \param tt Thermite tensor
!> \param elms The elements to point to
SUBROUTINE tt_assign_elms(tt,elms)
implicit none
TYPE(TT_TYPE),intent(INOUT) :: tt
real(realk),pointer         :: elms(:)
integer :: ndim
IF (.NOT.associated(elms)) call lsquit('Error in tt_assign_elms: elms not assigned',-1)
ndim = sizeof(elms)
IF (.NOT.tt%nelms.NE.ndim) THEN
  write(*,*) 'Error in tt_assign_elms: nelms=',tt%nelms,' ndim=',ndim
  call lsquit('Error in tt_assign_elms: mismatch of nelms and ndim',-1)
ENDIF
tt%elms => elms
tt%elms_status = elms_status_assigned
END SUBROUTINE tt_assign_elms

!> Copies the elements into the Thermite tensor elements
!> \param tt Thermite tensor
!> \param elms The elements to be copied
SUBROUTINE tt_copy_elms_full(tt,elms)
implicit none
TYPE(TT_TYPE),intent(INOUT) :: tt
real(realk),pointer         :: elms(:)
!
integer :: ndim
!
IF (.NOT.associated(elms)) call lsquit('Error in tt_copy_elms_full: elms not assigned',-1)
ndim = sizeof(elms)
IF (.NOT.tt%nelms.NE.ndim) THEN
  write(*,*) 'Error in tt_copy_elms_full: nelms=',tt%nelms,' ndim=',ndim
  call lsquit('Error in tt_copy_elms_full: mismatch of nelms and ndim',-1)
ENDIF
!Allocates elements if they are not already allocated
IF (tt%elms_status.NE.elms_status_allocated) THEN
  call mem_alloc(tt%elms,tt%nelms)
  tt%elms_status = elms_status_allocated
ENDIF
call dcopy(tt%nelms,elms,1,tt%elms,1)
END SUBROUTINE tt_copy_elms_full

!> Copies a subblock of elements into the Thermite tensor
!> \param tt Thermite tensor
!> \param elms The elements to be copied
!> \param nsub The number of subblock indeces
!> \param subIndex The list of tensor indeces with subblock (rather than full dimensional) copying
!> \param subStart The start index for each subblock
!> \param subEnd   The end index for each subblock
SUBROUTINE tt_copy_elms_subblock(tt,elms,nsub,subIndex,subStart,subEnd)
implicit none
TYPE(TT_TYPE),intent(INOUT) :: tt
real(realk),pointer         :: elms(:)
integer,intent(IN)          :: nsub
integer,intent(IN)          :: subIndex(nsub)
integer,intent(IN)          :: subStart(nsub)
integer,intent(IN)          :: subEnd(nsub)
!
integer :: ndim
!
IF (.NOT.associated(elms)) call lsquit('Error in tt_copy_elms_subblock: elms not assigned',-1)

!Allocates all elements if they are not already allocated
IF (tt%elms_status.NE.elms_status_allocated) THEN
  call mem_alloc(tt%elms,tt%nelms)
  tt%elms_status = elms_status_allocated
ENDIF
!call dcopy(tt%nelms,elms,1,tt%elms,1)
call lsquit('tt_copy_elms_subblock not yet implented',-1)
END SUBROUTINE tt_copy_elms_subblock

!> Contracts t1 with t2 to construct output o
!> 
!> SIMEN: Under construction
!> 
!> \param o Output tensor
!> \param t1 Tensor 1
!> \param t2 Tensor 2
!> \param t2 index2 contraction indeces
SUBROUTINE tt_contract(o,t1,t2,index2)
implicit none
TYPE(TT_TYPE),intent(INOUT) :: o
TYPE(TT_TYPE),intent(IN)    :: t1,t2
Integer,intent(IN)          :: index2(t2%ndim)
!
Integer :: index1(t1%ndim)
Integer :: i,j,nShared,iShared,nFree,nOut
Logical :: ok
type i_p
  integer,pointer :: p
end type i_p
type(i_p),pointer :: o_indx(:),t1_indx(:),t2_indx(:)
Integer,pointer   :: shared(:),si(:),out(:),oi(:)

!Count the shared indeces
nshared = 0
DO i=1,t1%ndim
  index1(i) = i
  DO j=1,t2%ndim
    IF (index2(j).EQ.i) nshared=nshared+1
  ENDDO
ENDDO
nFree = t2%ndim - nShared
nOut  = t1%ndim + t2%ndim - 2*nShared

!*************************** DEBUG SECTION START
#if DEBUG_TT
ok=.TRUE.
DO i=1,t2%ndim
  DO j=i+1,t2%ndim
   IF (index2(i).EQ.index2(j)) ok=.FALSE.
  ENDDO
ENDDO
IF (.not.ok) THEN
  write(*,*) 'Error in tt_contract: index2 should have different contraction indeces, index2=',index2
  call lsquit('Error in tt_contract: index2 should have different contraction indeces',-1)
ENDIF
IF (o%ndim.NE.t1%ndim+t2%ndim-nShared) THEN
  write(*,*) 'Error in tt_contract: inconsistent set of dimensions, o, t1, t2, nshared',o%ndim,t1%ndim,t2%ndim,nShared
  call lsquit('Error in tt_contract: inconsistent set of dimensions',-1)
ENDIF
DO i=1,t2%ndim
  IF ((index2(i).LE.0).OR.(index2(i).GT.t1%ndim+nFree)) THEN
    write(*,*) 'Error in tt_contract: contraction index error. Index outside range: n1=',t1%ndim,&
     & ', n2=',t2%ndim,', nFree=',nFree,', index2= ',index2
    call lsquit('Error in tt_contract: contraction index error. Index outside range',-1)
  ENDIF
ENDDO
#endif
!*************************** DEBUG SECTION END 

allocate(t1_indx(t1%ndim))
allocate(t2_indx(t2%ndim))
call mem_alloc(shared,nShared) !The dimension of the shared indeces
call mem_alloc(si,nShared)     !The shared loop index
iShared = 0
DO i=1,t1%ndim
  DO j=1,t2%ndim
    IF (index2(j).EQ.i) THEN
      iShared = iShared + 1
      shared(iShared) = t1%ao_info(i)%p%norb
    ENDIF
  ENDDO
ENDDO
DO i=1,t2%ndim
! t2_indx(i)%p => indx(index2(i))
ENDDO



call mem_dealloc(shared)
call mem_dealloc(si)
deallocate(t1_indx)
deallocate(t2_indx)

END SUBROUTINE tt_contract


END MODULE ThermiteTensor
