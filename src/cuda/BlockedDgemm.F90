!> @file
!> Wrapper to GPU DGEMM for matrices too large to fit on GPU
!> Ideally you should be able to call CublasXT but something is off 
!> \author Thomas Kjaergaard
module BlockDGEMMWrapper_module
  use precision
  use iso_c_binding, only: c_ptr
  use memory_handling
  use gpu_interfaces
#ifdef VAR_OPENACC
  use openacc
#endif

  public :: LSBLOCKEDHOSTDGEMM

  private
  
contains
  !> Purpose: Wrapper to GPU DGEMM for matrices too large to fit on GPU
  !> Author:  Thomas Kjaergaard
  !> Date:    April 2017
  subroutine LSBLOCKEDHOSTDGEMM(TA,TB,M,N,K,alpha,A,LDA,B,LDB,beta,C,LDC,cublas_handleIN)
    implicit none
    integer,intent(in)        :: M,N,K,LDA,LDB,LDC
    real(realk),intent(in)    :: alpha,beta
    character,intent(in)      :: TA,TB 
    real(realk),intent(in)    :: A(M*K) 
    real(realk),intent(in)    :: B(K*N) 
    real(realk),intent(inout) :: C(M*N)
    type(c_ptr),optional      :: cublas_handleIN
    !local variables
!#ifdef VAR_LSDEBUG
!    integer,parameter :: BS=7
!    integer,parameter :: BSdimM1=6
!#else
    integer,parameter :: BS=1024
    integer,parameter :: BSdimM1=1023
!#endif
    integer :: dimM2,dimN2,dimK2
    logical :: remainderM,remainderN,remainderK,useGPU
    integer :: BM,BN,BK,KK,MM,NN,modM,modN,modK
    real(realk) :: fac
    real(realk),pointer :: SmallA(:),SmallB(:),SmallC(:)
    ! cublas stuff
    type(c_ptr), target :: handle_cptr
    type(c_ptr) :: cublas_handle
    integer*4 :: stat
    !> async handle
#ifdef VAR_OPENACC
    integer(kind=acc_handle_kind) :: async_id
#ifdef VAR_PGI
    type(c_ptr), external         :: acc_get_cuda_stream
#endif
#else
    !Not defined VAR_OPENACC
    integer :: async_id
#endif
    if((TA.EQ.'N'.OR.TA.EQ.'n'))THEN
       IF(LDA.NE.M)call lsquit('LSBLOCKEDHOSTDGEMM M.NE.LDA',-1)
    else
       IF(LDA.NE.K)call lsquit('LSBLOCKEDHOSTDGEMM K.NE.LDA',-1)
    endif
    if((TB.EQ.'N'.OR.TB.EQ.'n'))THEN
       IF(LDB.NE.K)call lsquit('LSBLOCKEDHOSTDGEMM K.NE.LDB',-1)
    else
       IF(LDB.NE.N)call lsquit('LSBLOCKEDHOSTDGEMM N.NE.LDB',-1)
    endif
    IF(LDC.NE.M)call lsquit('LSBLOCKEDHOSTDGEMM M.NE.LDC',-1)
#ifdef VAR_OPENACC
    useGPU=.TRUE.
    async_id = acc_async_sync
    useGPU=.TRUE.
#else
    useGPU=.FALSE.
    async_id = 0
    useGPU=.FALSE.
#endif
#ifdef VAR_CUBLAS
    IF(present(cublas_handleIN))THEN
       cublas_handle = cublas_handleIN
    ELSE
       ! initialize the CUBLAS context
       stat = cublasCreate_v2(cublas_handle)
       handle_cptr = acc_get_cuda_stream(acc_async_sync)
       stat = cublasSetStream_v2 ( cublas_handle, handle_cptr )
    ENDIF
#endif     
    IF(.NOT.useGPU.OR.M*N+N*K+K*M.LT.3*124*124)THEN
       !Too small too be transported to GPU or no GPU available
       CALL DGEMM(TA,TB,M,N,K,alpha,A,LDA,B,LDB,beta,C,LDC)
    ELSEIF(M*N+N*K+K*M.LT.0.0625E0_realk*1.0E9_realk)THEN
       !Matrices Fit in GPU memory, less than (0.5GB)
       IF(ABS(beta).LT.1.0E-16_realk)THEN
          !$acc data present_or_copyin(A,B) present_or_copyout(C) 
          call ls_dgemm_acc(TA,TB,M,N,K,Alpha,A,LDA,B,LDB,0.0E0_realk,C,LDC,&
               & int((i8*M)*K,kind=8),int(K*(N*i8),kind=8),int(M*(N*i8),kind=8),&
               & async_id,cublas_handle)       
          !$acc end data
       ELSE
          !$acc data present_or_copyin(A,B) present_or_copy(C) 
          call ls_dgemm_acc(TA,TB,M,N,K,Alpha,A,LDA,B,LDB,Beta,C,LDC,&
               & int((i8*M)*K,kind=8),int(K*(N*i8),kind=8),int(M*(N*i8),kind=8),&
               & async_id,cublas_handle)       
          !$acc end data
       ENDIF
    ELSE
       dimM2=(M/bs)*bs
       dimN2=(N/bs)*bs
       dimK2=(K/bs)*bs
       
       modM=mod(M,bs)
       modN=mod(N,bs)
       modK=mod(K,bs)
       remainderM = modM.GT.0
       remainderN = modN.GT.0
       remainderK = modK.GT.0
       !BSdimM1 = BS - 1    
       if((TA.EQ.'N'.OR.TA.EQ.'n').AND.(TB.EQ.'N'.OR.TB.EQ.'n'))THEN

          ! ===========
          ! N N Version
          ! ===========
          
#ifndef VAR_CRAY          
          call mem_TurnONThread_Memory()
          !$OMP PARALLEL DEFAULT(NONE),PRIVATE(MM,NN,KK,BM,BN,BK,&
          !$OMP SmallA,SmallB,SmallC,fac) SHARED(dimM2,cublas_handle, &
          !$OMP dimN2,dimK2,M,N,K,A,B,C,beta,alpha,async_id,useGPU, &
          !$OMP remainderM,remainderN,remainderK,modM,modN,modK)
          call init_threadmemvar()
#endif          
          if(dimM2>0)then
             if(dimN2>0)then
                call mem_alloc(SmallC,BS*BS,'SmallCBlockDgemmNN')
             else
                call mem_alloc(SmallC,BS*modN,'SmallCBlockDgemmNN')
             endif
             if(dimK2>0)then
                call mem_alloc(SmallA,BS*BS,'SmallABlockDgemmNN')
             else
                call mem_alloc(SmallA,BS*modK,'SmallABlockDgemmNN')
             endif
          else
             if(dimN2>0)then
                call mem_alloc(SmallC,modM*BS,'SmallCBlockDgemmNN')
             else
                call mem_alloc(SmallC,modM*modN,'SmallCBlockDgemmNN')
             endif
             if(dimK2>0)then
                call mem_alloc(SmallA,modM*BS,'SmallABlockDgemmNN')
             else
                call mem_alloc(SmallA,modM*modK,'SmallABlockDgemmNN')
             endif
          endif
          if(dimK2>0)then
             if(dimN2>0)then
                call mem_alloc(SmallB,BS*BS,'SmallBBlockDgemmNN')
             else
                call mem_alloc(SmallB,BS*modN,'SmallBBlockDgemmNN')
             endif
          else
             if(dimN2>0)then
                call mem_alloc(SmallB,modK*BS,'SmallBBlockDgemmNN')
             else
                call mem_alloc(SmallB,modK*modN,'SmallBBlockDgemmNN')
             endif
          endif
          !$acc data create(SmallA,SmallB,SmallC)
          if(dimM2>0.and.dimN2>0)then
             !The Big square blocks of dimension BS
#ifndef VAR_CRAY          
             !$OMP DO COLLAPSE(2)
#endif          
             do BM=1,dimM2,BS
                do BN=1,dimN2,BS
                   fac = 0.0E0_realk
                   if(dimK2>0)then
                      do BK=1,dimK2,BS
                         do KK=0,BSdimM1
                            CALL DCOPY(BS,A(BM+(BK+KK-1)*M),1,SmallA(1+(1+KK-1)*BS),1)
                         enddo
                         do NN=0,BSdimM1
                            CALL DCOPY(BS,B(BK+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*BS),1)
                         enddo
                         IF(UseGPU)THEN
                            !$acc update device(SmallA,SmallB)
                            call ls_dgemm_acc('N','N',BS,BS,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS,&
                                 & int((i8*BS)*BS,kind=8),int(BS*(BS*i8),kind=8),int(BS*(BS*i8),kind=8),&
                                 & async_id,cublas_handle)
                         ELSE
                            CALL DGEMM('N','N',BS,BS,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS)
                         ENDIF
                         fac = 1.0E0_realk
                      enddo
                   endif
                   if(remainderK)then
                      !The remainder in the K dimension
                      do KK=1,modK
                         CALL DCOPY(BS,A(BM+(dimK2+KK-1)*M),1,SmallA(1+(KK-1)*BS),1)
                      enddo
                      do NN=0,BSdimM1
                         CALL DCOPY(modK,B(dimK2+1+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*modK),1)
                      enddo
                      IF(UseGPU)THEN
                         !$acc update device(SmallA,SmallB)
                         call ls_dgemm_acc('N','N',BS,BS,modK,Alpha,SmallA,BS,SmallB,modK,fac,SmallC,BS,&
                              & int((i8*BS)*modK,kind=8),int(modK*(BS*i8),kind=8),int(BS*(BS*i8),kind=8),&
                              & async_id,cublas_handle)
                      ELSE
                         CALL DGEMM('N','N',BS,BS,modK,Alpha,SmallA,BS,SmallB,modK,fac,SmallC,BS)
                      ENDIF
                   endif
                   !$acc update host(SmallC)
                   IF(ABS(beta).LT.1.0E-16_realk)THEN
                      do NN=0,BSdimM1
                         CALL DCOPY(BS,SmallC(1+(1+NN-1)*BS),1,C(BM+(BN+NN-1)*M),1)
                      enddo
                   ELSE
                      do NN=0,BSdimM1
                         do MM=0,BSdimM1
                            C(BM+MM+(BN+NN-1)*M) = beta*C(BM+MM+(BN+NN-1)*M) + SmallC(1+MM+(1+NN-1)*BS)
                         enddo
                      enddo
                   ENDIF
                enddo
             enddo
#ifndef VAR_CRAY          
             !$OMP END DO NOWAIT
#endif          
          endif
          if(remainderM.and.dimN2>0)then
             !The remainder in the M dimension
#ifndef VAR_CRAY          
             !$OMP DO
#endif          
             do BN=1,dimN2,BS
                fac = 0.0E0_realk
                if(dimK2>0)then
                   do BK=1,dimK2,BS                   
                      do KK=0,BSdimM1
                         CALL DCOPY(modM,A(dimM2+1+(BK+KK-1)*M),1,SmallA(1+(1+KK-1)*modM),1)
                      enddo
                      do NN=0,BSdimM1
                         CALL DCOPY(BS,B(BK+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*BS),1)
                      enddo
                      IF(UseGPU)THEN
                         !$acc update device(SmallA,SmallB)
                         call ls_dgemm_acc('N','N',modM,BS,BS,Alpha,SmallA,modM,SmallB,BS,fac,SmallC,modM,&
                              & int((i8*modM)*BS,kind=8),int(BS*(BS*i8),kind=8),int(modM*(BS*i8),kind=8),&
                              & async_id,cublas_handle)
                      ELSE
                         CALL DGEMM('N','N',modM,BS,BS,Alpha,SmallA,modM,SmallB,BS,fac,SmallC,modM)
                      ENDIF
                      fac = 1.0E0_realk
                   enddo
                endif
                if(remainderK)then
                   !The remainder in the K dimension
                   do KK=1,modK
                      CALL DCOPY(modM,A(dimM2+1+(dimK2+KK-1)*M),1,SmallA(1+(KK-1)*modM),1)
                   enddo
                   do NN=0,BSdimM1
                      CALL DCOPY(modK,B(dimK2+1+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*modK),1)
                   enddo
                   IF(UseGPU)THEN
                      !$acc update device(SmallA,SmallB)
                      call ls_dgemm_acc('N','N',modM,BS,modK,Alpha,SmallA,modM,SmallB,modK,fac,SmallC,modM,&
                           & int((i8*modM)*modK,kind=8),int(modK*(BS*i8),kind=8),int(modM*(BS*i8),kind=8),&
                           & async_id,cublas_handle)
                   ELSE
                      CALL DGEMM('N','N',modM,BS,modK,Alpha,SmallA,modM,SmallB,modK,fac,SmallC,modM)
                   ENDIF
                endif
                !$acc update host(SmallC)
                IF(ABS(beta).LT.1.0E-16_realk)THEN
                   do NN=0,BSdimM1
                      CALL DCOPY(modM,SmallC(1+(1+NN-1)*modM),1,C(dimM2+1+(BN+NN-1)*M),1)
                   enddo
                ELSE
                   do NN=0,BSdimM1
                      do MM=1,modM
                         C(dimM2+MM+(BN+NN-1)*M) = beta*C(dimM2+MM+(BN+NN-1)*M) + SmallC(MM+(1+NN-1)*modM)
                      enddo
                   enddo
                ENDIF
             enddo
#ifndef VAR_CRAY          
             !$OMP END DO NOWAIT
#endif          
          endif
          if(dimM2>0.and.remainderN)then
             !The remainder in the N dimension
#ifndef VAR_CRAY          
             !$OMP DO
#endif          
             do BM=1,dimM2,BS
                fac = 0.0E0_realk
                if(dimK2>0)then
                   do BK=1,dimK2,BS
                      do KK=0,BSdimM1
                         CALL DCOPY(BS,A(BM+(BK+KK-1)*M),1,SmallA(1+(1+KK-1)*BS),1)
                      enddo
                      do NN=1,modN
                         CALL DCOPY(BS,B(BK+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*BS),1)
                      enddo
                      IF(UseGPU)THEN
                         !$acc update device(SmallA,SmallB)
                         call ls_dgemm_acc('N','N',BS,modN,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS,&
                              & int((i8*BS)*BS,kind=8),int(BS*(modN*i8),kind=8),int(BS*(modN*i8),kind=8),&
                              & async_id,cublas_handle)
                      ELSE
                         CALL DGEMM('N','N',BS,modN,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS)
                      ENDIF
                      fac = 1.0E0_realk
                   enddo
                endif
                if(remainderK)then
                   !The remainder in the K dimension
                   do KK=1,modK
                      CALL DCOPY(BS,A(BM+(dimK2+KK-1)*M),1,SmallA(1+(KK-1)*BS),1)
                   enddo
                   do NN=1,modN
                      CALL DCOPY(modK,B(dimK2+1+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*modK),1)
                   enddo
                   IF(UseGPU)THEN
                      !$acc update device(SmallA,SmallB)
                      call ls_dgemm_acc('N','N',BS,modN,modK,Alpha,SmallA,BS,SmallB,modK,fac,SmallC,BS,&
                           & int((i8*BS)*modK,kind=8),int(modK*(modN*i8),kind=8),int(BS*(modN*i8),kind=8),&
                           & async_id,cublas_handle)
                   ELSE
                      CALL DGEMM('N','N',BS,modN,modK,Alpha,SmallA,BS,SmallB,modK,fac,SmallC,BS)
                   ENDIF
                endif
                !$acc update host(SmallC)
                IF(ABS(beta).LT.1.0E-16_realk)THEN
                   do NN=1,modN
                      CALL DCOPY(BS,SmallC(1+(NN-1)*BS),1,C(BM+(dimN2+NN-1)*M),1)
                   enddo
                ELSE
                   do NN=1,modN
                      do MM=0,BSdimM1
                         C(BM+MM+(dimN2+NN-1)*M) = beta*C(BM+MM+(dimN2+NN-1)*M) + SmallC(1+MM+(NN-1)*BS)
                      enddo
                   enddo
                ENDIF
             enddo
#ifndef VAR_CRAY          
             !$OMP END DO NOWAIT
#endif          
          endif
          if(remainderM.and.remainderN)then
             !The remainder of M AND N dimensions
             fac = 0.0E0_realk
             if(dimK2>0)then
#ifndef VAR_CRAY          
                !$OMP DO
#endif          
                do BK=1,dimK2,BS
                   do KK=0,BSdimM1
                      CALL DCOPY(modM,A(dimM2+1+(BK+KK-1)*M),1,SmallA(1+(1+KK-1)*modM),1)
                   enddo
                   do NN=1,modN
                      CALL DCOPY(BS,B(BK+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*BS),1)
                   enddo
                   IF(UseGPU)THEN
                      !$acc update device(SmallA,SmallB)
                      call ls_dgemm_acc('N','N',modM,modN,BS,Alpha,SmallA,modM,SmallB,BS,fac,SmallC,modM,&
                           & int((i8*modM)*BS,kind=8),int(BS*(modN*i8),kind=8),int(modM*(modN*i8),kind=8),&
                           & async_id,cublas_handle)
                   ELSE
                      CALL DGEMM('N','N',modM,modN,BS,Alpha,SmallA,modM,SmallB,BS,fac,SmallC,modM)
                   ENDIF
                   fac = 1.0E0_realk
                enddo
#ifndef VAR_CRAY          
                !$OMP END DO NOWAIT
#endif          
             endif
#ifndef VAR_CRAY          
             !$OMP SINGLE
#endif          
             if(remainderK)then
                !The remainder in the K dimension
                do KK=1,modK
                   CALL DCOPY(modM,A(dimM2+1+(dimK2+KK-1)*M),1,SmallA(1+(KK-1)*modM),1)
                enddo
                do NN=1,modN
                   CALL DCOPY(modK,B(dimK2+1+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*modK),1)
                enddo
                IF(UseGPU)THEN
                   !$acc update device(SmallA,SmallB)
                   call ls_dgemm_acc('N','N',modM,modN,modK,Alpha,SmallA,modM,SmallB,modK,fac,SmallC,modM,&
                        & int((i8*modM)*modK,kind=8),int(modK*(modN*i8),kind=8),int(modM*(modN*i8),kind=8),&
                        & async_id,cublas_handle)
                ELSE
                   CALL DGEMM('N','N',modM,modN,modK,Alpha,SmallA,modM,SmallB,modK,fac,SmallC,modM)
                ENDIF
             endif
             !$acc update host(SmallC)
             IF(ABS(beta).LT.1.0E-16_realk)THEN
                do NN=1,modN
                   CALL DCOPY(modM,SmallC(1+(NN-1)*modM),1,C(dimM2+1+(dimN2+NN-1)*M),1)
                enddo
             ELSE
                do NN=1,modN
                   do MM=1,modM          
                      C(dimM2+MM+(dimN2+NN-1)*M) = beta*C(dimM2+MM+(dimN2+NN-1)*M) + SmallC(MM+(NN-1)*modM)
                   enddo
                enddo
             ENDIF
#ifndef VAR_CRAY          
             !$OMP END SINGLE NOWAIT
#endif          
          endif
          !$acc end data
          call mem_dealloc(SmallA)
          call mem_dealloc(SmallB)
          call mem_dealloc(SmallC)
#ifndef VAR_CRAY          
          call collect_thread_memory()
          !$OMP END PARALLEL
          call mem_TurnOffThread_Memory()
#endif          
       elseif((TA.EQ.'T'.OR.TA.EQ.'t').AND.(TB.EQ.'N'.OR.TB.EQ.'n'))THEN

          ! ===========
          ! T N Version
          ! ===========
          
#ifndef VAR_CRAY          
          call mem_TurnONThread_Memory()
          !$OMP PARALLEL DEFAULT(NONE),PRIVATE(MM,NN,KK,BM,BN,BK,&
          !$OMP SmallA,SmallB,SmallC,fac) SHARED(dimM2,cublas_handle, &
          !$OMP dimN2,dimK2,M,N,K,A,B,C,beta,alpha,async_id,useGPU, &
          !$OMP remainderM,remainderN,remainderK,modM,modN,modK)
          call init_threadmemvar()
#endif          
          if(dimM2>0)then
             if(dimN2>0)then
                call mem_alloc(SmallC,BS*BS,'SmallCBlockDgemmNN')
             else
                call mem_alloc(SmallC,BS*modN,'SmallCBlockDgemmNN')
             endif
             if(dimK2>0)then
                call mem_alloc(SmallA,BS*BS,'SmallABlockDgemmNN')
             else
                call mem_alloc(SmallA,modK*BS,'SmallABlockDgemmNN')
             endif
          else
             if(dimN2>0)then
                call mem_alloc(SmallC,modM*BS,'SmallCBlockDgemmNN')
             else
                call mem_alloc(SmallC,modM*modN,'SmallCBlockDgemmNN')
             endif
             if(dimK2>0)then
                call mem_alloc(SmallA,BS*modM,'SmallABlockDgemmNN')
             else
                call mem_alloc(SmallA,modK*modM,'SmallABlockDgemmNN')
             endif
          endif
          if(dimK2>0)then
             if(dimN2>0)then
                call mem_alloc(SmallB,BS*BS,'SmallBBlockDgemmNN')
             else
                call mem_alloc(SmallB,BS*modN,'SmallBBlockDgemmNN')
             endif
          else
             if(dimN2>0)then
                call mem_alloc(SmallB,modK*BS,'SmallBBlockDgemmNN')
             else
                call mem_alloc(SmallB,modK*modN,'SmallBBlockDgemmNN')
             endif
          endif
          !$acc data create(SmallA,SmallB,SmallC)
          if(dimM2>0.and.dimN2>0)then
             !The Big square blocks of dimension BS
#ifndef VAR_CRAY          
             !$OMP DO COLLAPSE(2)
#endif
             do BM=1,dimM2,BS
                do BN=1,dimN2,BS
                   fac = 0.0E0_realk
                   if(dimK2>0)then
                      do BK=1,dimK2,BS
                         do MM=0,BSdimM1
                            CALL DCOPY(BS,A(BK+(BM+MM-1)*K),1,SmallA(1+(1+MM-1)*BS),1)
                         enddo
                         do NN=0,BSdimM1
                            CALL DCOPY(BS,B(BK+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*BS),1)
                         enddo
                         IF(UseGPU)THEN
                            !$acc update device(SmallA,SmallB)
                            call ls_dgemm_acc('T','N',BS,BS,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS,&
                                 & int((i8*BS)*BS,kind=8),int(BS*(BS*i8),kind=8),int(BS*(BS*i8),kind=8),&
                                 & async_id,cublas_handle)
                         ELSE
                            CALL DGEMM('T','N',BS,BS,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS)
                         ENDIF
                         fac = 1.0E0_realk
                      enddo
                   endif
                   if(remainderK)then
                      !The remainder in the K dimension
                      do MM=0,BSdimM1
                         CALL DCOPY(modK,A(dimK2+1+(BM+MM-1)*K),1,SmallA(1+(1+MM-1)*modK),1)
                      enddo
                      do NN=0,BSdimM1
                         CALL DCOPY(modK,B(dimK2+1+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*modK),1)
                      enddo
                      IF(UseGPU)THEN
                         !$acc update device(SmallA,SmallB)
                         call ls_dgemm_acc('T','N',BS,BS,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,BS,&
                              & int((i8*BS)*modK,kind=8),int(modK*(BS*i8),kind=8),int(BS*(BS*i8),kind=8),&
                              & async_id,cublas_handle)
                      ELSE
                         CALL DGEMM('T','N',BS,BS,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,BS)
                      ENDIF
                   endif
                   !$acc update host(SmallC)
                   IF(ABS(beta).LT.1.0E-16_realk)THEN
                      do NN=0,BSdimM1
                         CALL DCOPY(BS,SmallC(1+(1+NN-1)*BS),1,C(BM+(BN+NN-1)*M),1)
                      enddo
                   ELSE
                      do NN=0,BSdimM1
                         do MM=0,BSdimM1
                            C(BM+MM+(BN+NN-1)*M) = beta*C(BM+MM+(BN+NN-1)*M) + SmallC(1+MM+(1+NN-1)*BS)
                         enddo
                      enddo
                   ENDIF
                enddo
             enddo
#ifndef VAR_CRAY          
             !$OMP END DO NOWAIT
#endif
          endif
          if(remainderM.and.dimN2>0)then
             !The remainder in the M dimension
#ifndef VAR_CRAY          
             !$OMP DO
#endif
             do BN=1,dimN2,BS
                fac = 0.0E0_realk
                if(dimK2>0)then
                   do BK=1,dimK2,BS                   
                      do MM=1,modM
                         CALL DCOPY(BS,A(BK+(dimM2+MM-1)*K),1,SmallA(1+(MM-1)*BS),1)
                      enddo
                      do NN=0,BSdimM1
                         CALL DCOPY(BS,B(BK+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*BS),1)
                      enddo
                      IF(UseGPU)THEN
                         !$acc update device(SmallA,SmallB)
                         call ls_dgemm_acc('T','N',modM,BS,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,modM,&
                              & int((i8*modM)*BS,kind=8),int(BS*(BS*i8),kind=8),int(modM*(BS*i8),kind=8),&
                              & async_id,cublas_handle)
                      ELSE
                         CALL DGEMM('T','N',modM,BS,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,modM)
                      ENDIF
                      fac = 1.0E0_realk
                   enddo
                endif
                if(remainderK)then
                   !The remainder in the K dimension
                   do MM=1,modM
                      CALL DCOPY(modK,A(dimK2+1+(dimM2+MM-1)*K),1,SmallA(1+(MM-1)*modK),1)
                   enddo
                   do NN=0,BSdimM1
                      CALL DCOPY(modK,B(dimK2+1+(BN+NN-1)*K),1,SmallB(1+(1+NN-1)*modK),1)
                   enddo
                   IF(UseGPU)THEN
                      !$acc update device(SmallA,SmallB)
                      call ls_dgemm_acc('T','N',modM,BS,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,modM,&
                           & int((i8*modM)*modK,kind=8),int(modK*(BS*i8),kind=8),int(modM*(BS*i8),kind=8),&
                           & async_id,cublas_handle)
                   ELSE
                      CALL DGEMM('T','N',modM,BS,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,modM)
                   ENDIF
                endif
                !$acc update host(SmallC)
                IF(ABS(beta).LT.1.0E-16_realk)THEN
                   do NN=0,BSdimM1
                      CALL DCOPY(modM,SmallC(1+(1+NN-1)*modM),1,C(dimM2+1+(BN+NN-1)*M),1)
                   enddo
                ELSE
                   do NN=0,BSdimM1
                      do MM=1,modM
                         C(dimM2+MM+(BN+NN-1)*M) = beta*C(dimM2+MM+(BN+NN-1)*M) + SmallC(MM+(1+NN-1)*modM)
                      enddo
                   enddo
                ENDIF
             enddo
#ifndef VAR_CRAY          
             !$OMP END DO NOWAIT
#endif
          endif
          if(dimM2>0.and.remainderN)then
             !The remainder in the N dimension
#ifndef VAR_CRAY          
             !$OMP DO
#endif
             do BM=1,dimM2,BS
                fac = 0.0E0_realk
                if(dimK2>0)then
                   do BK=1,dimK2,BS
                      do MM=0,BSdimM1
                         CALL DCOPY(BS,A(BK+(BM+MM-1)*K),1,SmallA(1+(1+MM-1)*BS),1)
                      enddo
                      do NN=1,modN
                         CALL DCOPY(BS,B(BK+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*BS),1)
                      enddo
                      IF(UseGPU)THEN
                         !$acc update device(SmallA,SmallB)
                         call ls_dgemm_acc('T','N',BS,modN,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS,&
                              & int((i8*BS)*BS,kind=8),int(BS*(modN*i8),kind=8),int(BS*(modN*i8),kind=8),&
                              & async_id,cublas_handle)
                      ELSE
                         CALL DGEMM('T','N',BS,modN,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,BS)
                      ENDIF
                      fac = 1.0E0_realk
                   enddo
                endif
                if(remainderK)then
                   !The remainder in the K dimension
                   do MM=0,BSdimM1
                      CALL DCOPY(modK,A(dimK2+1+(BM+MM-1)*K),1,SmallA(1+(1+MM-1)*modK),1)
                   enddo
                   do NN=1,modN
                      CALL DCOPY(modK,B(dimK2+1+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*modK),1)
                   enddo
                   IF(UseGPU)THEN
                      !$acc update device(SmallA,SmallB)
                      call ls_dgemm_acc('T','N',BS,modN,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,BS,&
                           & int((i8*BS)*modK,kind=8),int(modK*(modN*i8),kind=8),int(BS*(modN*i8),kind=8),&
                           & async_id,cublas_handle)
                   ELSE
                      CALL DGEMM('T','N',BS,modN,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,BS)
                   ENDIF
                endif
                !$acc update host(SmallC)
                IF(ABS(beta).LT.1.0E-16_realk)THEN
                   do NN=1,modN
                      CALL DCOPY(BS,SmallC(1+(NN-1)*BS),1,C(BM+(dimN2+NN-1)*M),1)
                   enddo
                ELSE
                   do NN=1,modN
                      do MM=0,BSdimM1
                         C(BM+MM+(dimN2+NN-1)*M) = beta*C(BM+MM+(dimN2+NN-1)*M) + SmallC(1+MM+(NN-1)*BS)
                      enddo
                   enddo
                ENDIF
             enddo 
#ifndef VAR_CRAY          
             !$OMP END DO NOWAIT
#endif
          endif
          if(remainderM.and.remainderN)then
             !The remainder of M AND N dimensions
             fac = 0.0E0_realk
             if(dimK2>0)then
#ifndef VAR_CRAY
                !$OMP DO
#endif
                do BK=1,dimK2,BS
                   do MM=1,modM
                      CALL DCOPY(BS,A(BK+(dimM2+MM-1)*K),1,SmallA(1+(MM-1)*BS),1)
                   enddo
                   do NN=1,modN
                      CALL DCOPY(BS,B(BK+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*BS),1)
                   enddo
                   IF(UseGPU)THEN
                      !$acc update device(SmallA,SmallB)
                      call ls_dgemm_acc('T','N',modM,modN,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,modM,&
                           & int((i8*modM)*BS,kind=8),int(BS*(modN*i8),kind=8),int(modM*(modN*i8),kind=8),&
                           & async_id,cublas_handle)
                   ELSE
                      CALL DGEMM('T','N',modM,modN,BS,Alpha,SmallA,BS,SmallB,BS,fac,SmallC,modM)
                   ENDIF
                   fac = 1.0E0_realk
                enddo
#ifndef VAR_CRAY
                !$OMP END DO NOWAIT
#endif
             endif
#ifndef VAR_CRAY
             !$OMP SINGLE
#endif
             if(remainderK)then
                !The remainder in the K dimension
                do MM=1,modM
                   CALL DCOPY(modK,A(dimK2+1+(dimM2+MM-1)*K),1,SmallA(1+(MM-1)*modK),1)
                enddo
                do NN=1,modN
                   CALL DCOPY(modK,B(dimK2+1+(dimN2+NN-1)*K),1,SmallB(1+(NN-1)*modK),1)
                enddo
                IF(UseGPU)THEN
                   !$acc update device(SmallA,SmallB)
                   call ls_dgemm_acc('T','N',modM,modN,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,modM,&
                        & int((i8*modM)*modK,kind=8),int(modK*(modN*i8),kind=8),int(modM*(modN*i8),kind=8),&
                        & async_id,cublas_handle)
                ELSE
                   CALL DGEMM('T','N',modM,modN,modK,Alpha,SmallA,modK,SmallB,modK,fac,SmallC,modM)
                ENDIF
             endif
             !$acc update host(SmallC)
             IF(ABS(beta).LT.1.0E-16_realk)THEN
                do NN=1,modN
                   CALL DCOPY(modM,SmallC(1+(NN-1)*modM),1,C(dimM2+1+(dimN2+NN-1)*M),1)
                enddo
             ELSE
                do NN=1,modN
                   do MM=1,modM          
                      C(dimM2+MM+(dimN2+NN-1)*M) = beta*C(dimM2+MM+(dimN2+NN-1)*M) + SmallC(MM+(NN-1)*modM)
                   enddo
                enddo
             ENDIF
#ifndef VAR_CRAY
             !$OMP END SINGLE NOWAIT
#endif
          endif
          !$acc end data
          call mem_dealloc(SmallA)
          call mem_dealloc(SmallB)
          call mem_dealloc(SmallC)
#ifndef VAR_CRAY
          call collect_thread_memory()
          !$OMP END PARALLEL
          call mem_TurnOffThread_Memory()       
#endif
       else
          call lsquit('LSBLOCKEDHOSTDGEMM only N N and TN version implemented',-1)
       endif
    ENDIF

#ifdef VAR_CUBLAS    
    IF(.NOT.present(cublas_handleIN))THEN
       ! Destroy the CUBLAS context
       stat = cublasDestroy_v2(cublas_handle)    
    ENDIF
#endif
  end subroutine LSBLOCKEDHOSTDGEMM

end module BlockDGEMMWrapper_module

