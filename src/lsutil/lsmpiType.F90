module lsmpi_type
  use lsmpi_param
  use lsmpi_Bcast
  use lsmpi_ReductionMod
  use precision
  use lsmpi_typeParam
  use,intrinsic :: iso_c_binding,only:c_ptr,c_f_pointer,c_associated,&
       & c_null_ptr,c_loc
  use LSparameters
  use memory_handling, only: mem_alloc,mem_dealloc, max_mem_used_global,&
       & longintbuffersize, print_maxmem, stats_mem, copy_from_mem_stats,&
       & init_globalmemvar, stats_mpi_mem, copy_to_mem_stats, &
       & MemModParamPrintMemory, Print_Memory_info, &
       & MemModParamPrintMemorylupri, mem_allocated_global
#ifdef VAR_MPI
  use infpar_module
  use lsmpi_module
#endif

  public :: MaxIncreaseSize,&
       & get_size_for_comm,init_mpi_groups,&
       & init_mpi_groups_general,init_mpi_subgroup,&
       & create_mpi_local_group_structure,print_mpi_group_info,&
       & divide_local_mpi_group,lsmpi_print,lsmpi_default_mpi_group,&
       & lsmpi_print_mem_info,lsmpi_finalize,lsmpi_comm_free,&
       & init_mpi_groups_slave,lsmpi_iprobe,lsmpi_probe

private

contains
  !> Get number of processors for a specific communicator
  !> \author Thomas Kjaergaard
  !> \date juni 2012
  subroutine get_size_for_comm(comm,nodtot)
    implicit none
    !> Communicator
    integer(kind=ls_mpik),intent(in) :: comm
    !> Rank number in communicator group
    integer(kind=ls_mpik),intent(inout) :: nodtot
    integer(kind=ls_mpik) :: ierr
    ierr=0
#ifdef VAR_MPI
    call MPI_COMM_SIZE(comm,nodtot,ierr)
#else
    nodtot = 1
#endif
    if(ierr/=0) then
       call lsquit('get_size_for_comm: Something wrong!',-1)
    end if
  end subroutine get_size_for_comm

#ifdef VAR_MPI
  !> \brief Initialize MPI groups by setting information in the global
  !> type infpar. In particular, 
  !> infpar%lg_mynum, infpar%lg_nodtot, and infpar%lg_comm are set (see par_mod.f90).
  !> \author Kasper Kristensen
  !> \date March 2012
  subroutine init_mpi_groups(groupsize,lupri)
    implicit none
    !> Size of each group
    integer(kind=ls_mpik),intent(in) :: groupsize
    !> File unit number for LSDALTON.OUT
    integer,intent(in) :: lupri
    call init_mpi_groups_general(infpar%lg_nodtot,infpar%lg_mynum,infpar%lg_comm,groupsize,lupri)
  end subroutine init_mpi_groups

  !> \brief Initialize MPI groups by setting information in the global
  !> type infpar. In particular, 
  !> infpar%lg_mynum, infpar%lg_nodtot, and infpar%lg_comm are set (see par_mod.f90).
  !> \author Kasper Kristensen
  !> \date March 2012
  subroutine init_mpi_groups_general(lg_nodtot,lg_mynum,lg_comm,groupsize,lupri)
    implicit none
    !> Size of each group
    integer(kind=ls_mpik),intent(in) :: groupsize
    !> File unit number for LSDALTON.OUT
    integer,intent(in) :: lupri
    integer(kind=ls_mpik),intent(inout) :: lg_nodtot,lg_mynum,lg_comm

    integer :: i
    integer(kind=ls_mpik) :: ierr,ngroups,worldgroup,localgroup
    integer(kind=ls_mpik) :: mygroup,hstatus
    type(mpigroup),allocatable :: lg(:)
    CHARACTER*(MPI_MAX_PROCESSOR_NAME) ::  hname
    integer(kind=ls_mpik) :: gmsize
    integer(kind=ls_mpik),pointer :: gmranks(:)
    IERR=0


    ! CONVENTION FOR CREATING GROUPS
    ! ******************************

    ! For now, the groups have equal size if the input groupsize is a multiplum of
    ! the total number of nodes disregarding the master node (infpar%nodtot-1).
    ! Otherwise the last local group will be correspondingly larger than the local groups.
    ! For simplicity, the master rank (0) gets special treatment and is not assigned to any
    ! local group.

    ! Example 1:  7 nodes in total (ranks 0,1,2,3,4,5,6); groupsize=2
    ! ---------------------------------------------------------------
    ! 
    ! There is one master (rank 0) and 3 local groups of size 2 (lg_nodtot=2) with ranks:
    ! First  local group: (1,2)    - rank numbers inside local group (lg_mynum): (0,1)
    ! Second local group: (3,4)    - rank numbers inside local group (lg_mynum): (0,1)
    ! Third  local group: (5,6)    - rank numbers inside local group (lg_mynum): (0,1)
    ! The local groups talk together via lg_comm.
    ! 
    ! Example 2:  8 nodes in total (ranks 0,1,2,3,4,5,6,7); groupsize=2
    ! -----------------------------------------------------------------
    !
    ! Same principle as above, except that now there will be three local groups 
    ! of size 2,2, and 3:
    ! First  local group: (1,2)    - rank numbers inside local group (lg_mynum): (0,1)
    ! Second local group: (3,4)    - rank numbers inside local group (lg_mynum): (0,1)
    ! Third  local group: (5,6,7)  - rank numbers inside local group (lg_mynum): (0,1,2)

    ! Sanity check 1: Size of group is smaller than total number of nodes
    ! (Maximum allowed group size is infpar%nodtot-1 because 1 node is reserved for master)
    ! Groupsize of course also have to be positive.
    if( (groupsize > infpar%nodtot-1) .or. (groupsize<1) ) then
       print *, 'Rank = ', infpar%mynum
       print *, 'Requested groupsize = ', groupsize
       print *, 'Number of nodes (excluding master)', infpar%nodtot-1
       call lsquit('init_mpi_groups: Requested group size is unacceptable!',-1)
    end if

    ! Sanity check 2: Groups only meaningful if there are least two nodes
    if(infpar%nodtot < 2) then
       print *, 'Rank = ', infpar%mynum
       print *, 'Number of nodes = ', infpar%nodtot
       call lsquit('init_mpi_groups: Cannot create groups - less than two nodes!',-1)
    end if


    ! For MASTER rank
    ! ===============
    if(infpar%mynum.EQ.infpar%master) then
       ! Wake up slaves to call this routine
       call ls_mpibcast(GROUPINIT,infpar%master,MPI_COMM_LSDALTON)

       ! Bcast groupsize to slave
       call ls_mpibcast(groupsize,infpar%master,MPI_COMM_LSDALTON)
    end if

    ! Extract group handle for LSDALTON group (world group)
    call MPI_COMM_GROUP(MPI_COMM_LSDALTON, worldgroup, ierr)


    ! **************************************************
    ! *                   Local groups                 *
    ! **************************************************

    ! Number of local groups
    ngroups = floor(real(infpar%nodtot-1)/real(groupsize))

    ! Local group structure
    allocate(lg(ngroups))
    call create_mpi_local_group_structure(groupsize,ngroups,mygroup,lg)

    ! For global master, only include master itself
    gmsize = 1
    call mem_alloc(gmranks,gmsize)
    gmranks=0

    ! Create local MPI group for this rank
    if(infpar%mynum /= infpar%master) then
       call MPI_GROUP_INCL(worldgroup, lg(mygroup)%groupsize, lg(mygroup)%ranks, localgroup, ierr)
    else
       ! Master is assigned its own group.
       call MPI_GROUP_INCL(worldgroup, gmsize, gmranks, localgroup, ierr)
    end if

    if(infpar%mynum .EQ. infpar%master) then
       !Master writes info concerning groups and groupsizes
       WRITE(lupri,'(A)')' MPI Group Information'
       IF(ngroups.EQ.1)THEN
          WRITE(lupri,'(A,I6)')' Only 1 MPI group of GROUPSIZE: ',lg(1)%groupsize
       ELSE
          WRITE(lupri,'(A,I6,A,I6)')' Number of Groups with MPI GROUPSIZE of ',groupsize,':',ngroups-1
          WRITE(lupri,'(A,I6,A,I6)')' Number of Groups with MPI GROUPSIZE of ',lg(ngroups)%groupsize,':',1
       ENDIF
    endif

    call mem_dealloc(gmranks)

    ! Create local MPI communicator
    call MPI_COMM_CREATE(MPI_COMM_LSDALTON, localgroup, lg_comm, ierr)

    ! Done with group information for local group (information is now contained in communicator)
    call MPI_GROUP_FREE(localgroup,ierr)

    if(infpar%mynum /= infpar%master) then

       ! Set global parameters for local group
       ! -------------------------------------

       ! number of nodes in group
       lg_nodtot = lg(mygroup)%groupsize   

       ! Rank within group
       call get_rank_for_comm(lg_comm,lg_mynum)

       ! Print out
       CALL MPI_GET_PROCESSOR_NAME(HNAME,HSTATUS,IERR)
       print '(a,4i6,1X,a)', 'World rank, local rank, group, groupsize, host:', infpar%mynum, &
            & lg_mynum, mygroup, lg(mygroup)%groupsize, hname

    else  ! Global master
       lg_nodtot = gmsize
       call get_rank_for_comm (lg_comm,lg_mynum)
    end if

    call MPI_GROUP_FREE(worldgroup,ierr)
    do i=1,ngroups
       deallocate(lg(i)%ranks)
       nullify(lg(i)%ranks)
    end do
    deallocate(lg)

  end subroutine init_mpi_groups_general

  !> \brief Initialize a MPI group, which is a subset of the global_comm communicator
  !> This routine should be call by all nodes - this routine will not wake up the slaves
  !> \author Thomas Kjaergaard
  !> \date March 2014
  subroutine init_mpi_subgroup(lg_nodtot,lg_mynum,lg_comm,lg_member,groupsize,global_comm,lupri)
    implicit none
    !> Size of each group
    integer(kind=ls_mpik),intent(in) :: groupsize
    !> File unit number for LSDALTON.OUT
    integer,intent(in) :: lupri
    logical,intent(inout) :: lg_member
    integer(kind=ls_mpik),intent(in) :: global_comm
    integer(kind=ls_mpik),intent(inout) :: lg_nodtot,lg_mynum,lg_comm
    !local variables
    integer :: i
    integer(kind=ls_mpik) :: ierr,ngroups,worldgroup,localgroup
    integer(kind=ls_mpik) :: mygroup,hstatus
    type(mpigroup),allocatable :: lg(:)
    CHARACTER*(MPI_MAX_PROCESSOR_NAME) ::  hname
    integer(kind=ls_mpik) :: gmsize
    integer(kind=ls_mpik),pointer :: gmranks(:)
    IERR=0
    !sanity check 
    if( (groupsize .GE. infpar%nodtot) .or. (groupsize.LT.1) ) then
       print *, 'Rank =                ', infpar%mynum
       print *, 'Requested groupsize = ', groupsize
       print *, 'Number of nodes =     ', infpar%nodtot
       call lsquit('init_mpi_subgroup: Requested group size is unacceptable!',-1)
    end if

    ! Extract group handle for global group (world group)
    call MPI_COMM_GROUP(global_comm, worldgroup, ierr)


    ! **************************************************
    ! *                   Local groups                 *
    ! **************************************************

    ! Number of local groups = 2 (members and non members)
    ngroups = 2
    ! Local group structure
    allocate(lg(ngroups))

    lg(1)%groupsize = groupsize
    nullify(lg(1)%ranks)
    allocate(lg(1)%ranks(lg(1)%groupsize))
    do i = 1,lg(1)%groupsize
       lg(1)%ranks(i) = i-1
    enddo

    ! Assign all the remaining nodes to the non member  local group
    lg(2)%groupsize = infpar%nodtot - groupsize
    nullify(lg(2)%ranks)
    allocate(lg(2)%ranks(lg(2)%groupsize))
    do i = 1,lg(2)%groupsize
       lg(2)%ranks(i) = groupsize+i-1
    enddo
    IF(infpar%mynum.LT.groupsize)THEN
       mygroup = 1
       lg_member = .TRUE.
    ELSE
       mygroup = 2
       lg_member = .FALSE.
    ENDIF
    call MPI_GROUP_INCL(worldgroup, lg(mygroup)%groupsize, lg(mygroup)%ranks, localgroup, ierr)

    ! Create local MPI communicator
    call MPI_COMM_CREATE(global_comm, localgroup, lg_comm, ierr)
    ! Done with group information for local group (information is now contained in communicator)
    call MPI_GROUP_FREE(localgroup,ierr)

    ! Set global parameters for local group
    ! -------------------------------------
    ! number of nodes in group
    lg_nodtot = lg(mygroup)%groupsize   
    ! Rank within group
    call get_rank_for_comm(lg_comm,lg_mynum)

    ! Print out
    CALL MPI_GET_PROCESSOR_NAME(HNAME,HSTATUS,IERR)
    print '(a,4i6,1X,a)', 'World rank, local subrank, group, groupsize, host:', infpar%mynum, &
         & lg_mynum, mygroup, lg(mygroup)%groupsize, hname

    call MPI_GROUP_FREE(worldgroup,ierr)
    do i=1,2
       deallocate(lg(i)%ranks)
       nullify(lg(i)%ranks)
    end do
    deallocate(lg)

  end subroutine init_mpi_subgroup

  !> \brief Create simple mpigroup structure for local groups (see init_mpi_groups for details).
  !> \author Kasper Kristensen
  !> \date March 2012
  subroutine create_mpi_local_group_structure(groupsize,ngroups,mygroup,lg)
    implicit none

    !> Requested group size (the last group may be larger than this, see init_mpi_groups)
    integer(kind=ls_mpik),intent(in) :: groupsize
    !> Number of groups
    integer(kind=ls_mpik),intent(in) :: ngroups
    !> Group number for rank under consideration
    integer(kind=ls_mpik),intent(inout) :: mygroup
    !> Local group information (size and list of ranks in each group)
    type(mpigroup),intent(inout) :: lg(ngroups)
    integer(kind=ls_mpik) :: i,j
    integer(kind=ls_mpik) :: cnt


    ! Loop over groups
    cnt=0
    mygroup=-1
    do i=1,ngroups
       if(i.EQ.ngroups) then
          ! Assign all the remaining nodes (except master) to the same local group
          lg(i)%groupsize = infpar%nodtot-1 - (ngroups-1)*groupsize
       else
          ! Input group size
          lg(i)%groupsize = groupsize
       end if
       nullify(lg(i)%ranks)
       allocate(lg(i)%ranks(lg(i)%groupsize))

       ! Define ranks in local group (see init_mpi_groups)
       do j=1,lg(i)%groupsize
          cnt = cnt+1
          lg(i)%ranks(j) = cnt
          if(cnt.EQ.infpar%mynum) mygroup=i
       end do

    end do

    ! Master (cnt=0) is not included in any local group
    if(infpar%mynum.EQ.infpar%master) mygroup=0

    ! Check
    if(cnt /= infpar%nodtot-1) then
       call lsquit('create_mpi_group_structure: &
            & Something wrong with accnting in group creation ',-1)
    end if
    if(mygroup.EQ.-1) then
       print *, 'rank = ', infpar%mynum
       call lsquit('create_mpi_group_structure: &
            & Group number for rank not identified!',-1)
    end if


  end subroutine create_mpi_local_group_structure


  ! \brief Print information about MPI master group and local group.
  !> Only intended to be called from the main master rank.
  ! \author Kasper Kristensen
  ! \date March 2012
  subroutine print_mpi_group_info(ngroups,lg,lupri)

    implicit none

    !> Number of groups
    integer,intent(in) :: ngroups
    !> Local group information 
    type(mpigroup),intent(in) :: lg(ngroups)
    !> File unit number for LSDALTON.OUT
    integer,intent(in) :: lupri
    integer :: j

    write(lupri,*) 
    write(lupri,*) 
    write(lupri,*) '********************************************************'
    write(lupri,*) '*                  MPI group information               *'
    write(lupri,*) '********************************************************'
    write(lupri,'(1X,a,i6,a)') 'There are ', ngroups, ' local groups'
    write(lupri,*) 
    write(lupri,*) 
    write(lupri,*) 'LOCAL GROUPS'
    write(lupri,*) '============'
    do j=1,ngroups
       write(lupri,*) 
       write(lupri,'(1X,a,i6)') 'Group:', j
       write(lupri,*) '--------------------'
       write(lupri,'(1X,a,i6)') 'Groupsize:', lg(j)%groupsize
       write(lupri,'(1X,a,1000i6)') 'Ranks:', lg(j)%ranks
    end do

  end subroutine print_mpi_group_info

  !> \brief Divide local MPI group into ngroups smaller of dimensions defined in input.
  !> To do this the global parameters infpar%lg_mynum, infpar%lg_nodtot, and infpar%lg_comm 
  !> are redefined here.
  !> It is important to ensure that ALL MEMBERS of the local group call
  !> this routine at the same time - if not, some ranks will be waiting for a signal
  !> which never comes...
  !> In other words, synchronization of all nodes in the local groups must be done OUTSIDE this
  !> routine because their communication channel is redefined in here!
  !> \author Kasper Kristensen
  !> \date May 2012
  subroutine divide_local_mpi_group(ngroups,groupdims,print_)
    implicit none

    !> Number of new local groups to create from existing local group
    integer(kind=ls_mpik),intent(in) :: ngroups
    logical, intent(in), optional :: print_
    !> Dimensions for these groups
    !> NOTE: The sum of these dimensions must equal the current group size:
    !> sum(groupsize) = infpar%lg_nodtot
    integer(kind=ls_mpik),dimension(ngroups),intent(in) :: groupdims
    integer(kind=ls_mpik) :: old_nodtot, old_mynum, oldgroup, ierr, newgroup,I
    integer(kind=ls_mpik), allocatable :: mygroup(:)
    integer(kind=ls_mpik) :: mysize,offset,cnter,mygroupnumber,newcomm
    logical :: prnt
      IERR=0

    prnt = .false.
    if(present(print_)) prnt = print_
    ! EXAMPLE
    ! *******
    ! Assume that the current local group size is 7 and that we want
    ! to create two new groups of sizes 4 and 3:
    !
    ! ngroups = 2
    ! groupdims(1) = 4
    ! groupdims(2) = 3
    !
    ! The ranks are then reassigned to local groups as follows:
    !    Current local rank number               New local rank number 
    !    0 (groupsize=7)                         0  (new subgroup 1, new groupsize=4)
    !    1 (groupsize=7)                         1  (new subgroup 1, new groupsize=4)
    !    2 (groupsize=7)                         2  (new subgroup 1, new groupsize=4)
    !    3 (groupsize=7)                         3  (new subgroup 1, new groupsize=4)
    !    4 (groupsize=7)                         0  (new subgroup 2, new groupsize=3)
    !    5 (groupsize=7)                         1  (new subgroup 2, new groupsize=3)
    !    6 (groupsize=7)                         2  (new subgroup 2, new groupsize=3)
    !
    ! Local rank number is stored in infpar%lg_mynum,
    ! and the local group size is stored in infpar%lg_nodtot.
    ! The local groups communicate vta infpar%lg_comm
    ! These three global parameters are all modified here to describe the new local group.


    ! Save existing local group information before overwriting
    old_mynum = infpar%lg_mynum
    old_nodtot = infpar%lg_nodtot

    ! Sanity check: Sum of new group sizes equals old group size
    if(sum(groupdims) /= old_nodtot) then
       print *, 'Old size = ', old_nodtot
       print *, 'New sizes= ', groupdims
       call lsquit('divide_local_mpi_group: Sum of new group sizes &
            & does not equal old group size!',-1)
    end if


    ! Determine new local group for this rank
    ! =======================================
    cnter=0
    mysize=0
    do i=1,ngroups
       cnter = cnter + groupdims(i)
       if(old_mynum < cnter) then  

          ! this rank belongs to new group i (see example above)
          mygroupnumber  = i   

          ! size of new group
          mysize = groupdims(i)  

          ! number of ranks in other new groups having smaller group number
          offset = cnter - mysize
          exit

       end if
    end do

    if(mysize.EQ.0) call lsquit('Something wrong in divide local groups',-1)
    allocate(mygroup(mysize))

    ! Define new local group
    do i=1,mysize
       ! Ranks in new group defined as [offset;offset+mysize]
       ! and then we subtract 1 to start cnting from rank 0 in old local group.
       mygroup(i) = offset + i - 1
    end do



    ! Create new group
    ! ================

    ! Extract group handle (oldgroup) for existing local group
    call MPI_COMM_GROUP(infpar%lg_comm, oldgroup, ierr)

    ! Create group handle (newgroup) for new local group
    call MPI_GROUP_INCL(oldgroup, mysize, mygroup, newgroup, ierr)

    call MPI_COMM_CREATE(infpar%lg_comm, newgroup, newcomm, ierr)


    ! Done with group information for local group (information is now in communicator, newcomm)
    call MPI_GROUP_FREE(newgroup,ierr)
    call MPI_GROUP_FREE(oldgroup,ierr)
    ! Free old communcator
    call MPI_COMM_FREE(infpar%lg_comm, IERR)


    ! Set global parameters for new local group (overwrite old ones)
    ! ==============================================================

    ! number of nodes in group
    infpar%lg_nodtot = mysize

    ! Rank within group
    call get_rank_for_comm(newcomm,infpar%lg_mynum)

    ! Communicator
    infpar%lg_comm = newcomm

    if(infpar%lg_mynum==0.and.prnt) then
       print *, 'Redefining local group'
       print *, '======================'
       print '(1X,a,4i6)', 'Old rank, new rank, old size, new size ', &
            & old_mynum, infpar%lg_mynum, old_nodtot, infpar%lg_nodtot
       print *, 'New group = ', mygroup
    end if

    deallocate(mygroup)

  end subroutine divide_local_mpi_group
#endif

    subroutine lsmpi_print(lupri)
    implicit none
    integer :: lupri    
#ifdef VAR_MPI
    write(lupri,'(3X,A,I7,A)')'This is an MPI run using ',infpar%nodtot,' processes.'
    write(lupri,*)''
#endif
  end subroutine lsmpi_print

    !> \brief Set rank, sizes and communcators for local groups
    !> to be identical to those for world group.
    !> \author Kasper Kristensen
    !> \date March 2012
    subroutine lsmpi_default_mpi_group

#ifdef VAR_MPI
      implicit none

      infpar%lg_comm = MPI_COMM_LSDALTON
      infpar%lg_mynum = infpar%mynum
      infpar%lg_nodtot = infpar%nodtot      
#endif

    end subroutine lsmpi_default_mpi_group


    subroutine lsmpi_print_mem_info(lupri,mastercall)
      implicit none
      integer,intent(in) :: lupri
      logical,intent(in) :: mastercall
#ifdef VAR_MPI
      integer(kind=ls_mpik) :: o,n,ierr,I,t(1),tag_meminfo,cnt,dest,tag,from,root
      integer(kind=long) :: recvbuffer
      real(realk) :: recvbuffer_real
      integer(kind=long),pointer :: longintbufferInt(:) 
      integer(KIND=long) :: MPImem_allocated_global
#ifdef VAR_INT64
      integer, parameter :: i2l = 1
#else
      integer, parameter :: i2l = 2
#endif
      IERR=0

      tag=2001
      dest=0
      t=0
      o=1;n=0

      if ((infpar%mynum.eq.infpar%master).and.mastercall) THEN
         !wake up slaves
         call ls_mpibcast(LSMPIPRINTINFO,infpar%master,MPI_COMM_LSDALTON)
      ENDIF

      cnt = 1
      root = 0
      recvbuffer = 0
      !Total max_mem_used_global across all nodes
      CALL MPI_REDUCE(max_mem_used_global,recvbuffer,&
           & cnt,MPI_INTEGER8,MPI_SUM,root,MPI_COMM_LSDALTON,IERR)
      !IF direct communication is used the unlock times are interesting

      call lsmpi_reduction(time_lsmpi_win_unlock,infpar%master,MPI_COMM_LSDALTON)
  
      IF(infpar%mynum.eq.infpar%master) THEN
         WRITE(lupri,'(4X,A)')'The total memory used across all MPI nodes'
         call print_maxmem(lupri,recvbuffer,'TOTAL')
         Write(lupri,'(4X,"time spent in unlock: ",f19.10)') time_lsmpi_win_unlock
      ENDIF
      !Largest max_mem_used_global including Master
      CALL MPI_REDUCE(max_mem_used_global,recvbuffer,&
           & cnt,MPI_INTEGER8,MPI_MAX,root,MPI_COMM_LSDALTON,IERR)
      IF(infpar%mynum.eq.infpar%master) THEN
         WRITE(lupri,'(4X,A)')'Largest memory allocated on a single MPI slave node (including Master)'
         call print_maxmem(lupri,recvbuffer,'TOTAL')
      ENDIF
      !Largest Slave max_mem_used_global (not including Master)      
      IF(infpar%mynum.EQ.infpar%master)max_mem_used_global=0
      CALL MPI_REDUCE(max_mem_used_global,recvbuffer,&
           & cnt,MPI_INTEGER8,MPI_MAX,root,MPI_COMM_LSDALTON,IERR)
      IF(infpar%mynum.eq.infpar%master) THEN
         WRITE(lupri,'(4X,A)')'Largest memory allocated on a single MPI slave node (exclusing Master)'
         call print_maxmem(lupri,recvbuffer,'TOTAL')
      ENDIF


      IF(infpar%mynum.NE.infpar%master)then
         call stats_mem(6)
         call mem_alloc(longintbufferInt,longintbuffersize)
         call copy_from_mem_stats(longintbufferInt)
         cnt=longintbuffersize
         CALL MPI_SEND(longintbufferInt,cnt,MPI_INTEGER8,dest,tag,MPI_COMM_LSDALTON,IERR)
         call mem_dealloc(longintbufferInt)

      ELSE          
         MPImem_allocated_global = 0
         DO I=1,infpar%nodtot-1
            call init_globalmemvar !WARNING removes all mem info on master 
            call mem_alloc(longintbufferInt,longintbuffersize)
            cnt = longintbuffersize
            CALL MPI_RECV(longintbufferInt,cnt,MPI_INTEGER8,I,tag,&
                 & MPI_COMM_LSDALTON,lsmpi_status,IERR)
            call copy_to_mem_stats(longintbufferInt)
            call mem_dealloc(longintbufferInt)
            WRITE(LUPRI,'(4X,"  Memory statistics for MPI node number ",i9," ")') I
            call stats_mpi_mem(lupri)
            MPImem_allocated_global = MPImem_allocated_global + mem_allocated_global
            call flush(lupri)
         ENDDO
         WRITE(LUPRI,'(4X,"  Allocated MPI memory a cross all slaves:  ",i9," byte  &
              &- Should be zero - otherwise a leakage is present")') MPImem_allocated_global
      ENDIF

!!$!#ifdef VAR_LSDEBUG
!!$!     cnt=1
!!$!     root = infpar%master
!!$!     recvbuffer = 0
!!$!     recvbuffer_real = 0.0E0_realk
!!$!     CALL MPI_REDUCE(poketime,recvbuffer_real,&
!!$!            & cnt,MPI_DOUBLE_PRECISION,MPI_SUM,root,MPI_COMM_LSDALTON,IERR)
!!$!     CALL MPI_REDUCE(poketimes,recvbuffer,&
!!$!            & cnt,MPI_INTEGER8,MPI_SUM,root,MPI_COMM_LSDALTON,IERR)
!!$!     if(infpar%mynum==infpar%master)then
!!$!       print *,"CUMULATIVE MPI POKETIME",recvbuffer_real,recvbuffer,recvbuffer_real/(recvbuffer*1.0E0_realk)
!!$!     endif
!!$!#endif     
#endif
    end subroutine lsmpi_print_mem_info

    subroutine lsmpi_finalize(lupri,mastercall)
      implicit none
      integer,intent(in)    :: lupri
      logical,intent(in)     :: mastercall
#ifdef VAR_MPI
      integer(kind=ls_mpik) :: ierr
      ierr = 0

      if ((infpar%mynum.eq.infpar%master).and.mastercall)&
       &call ls_mpibcast(LSMPIQUIT,infpar%master,MPI_COMM_LSDALTON)

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !CHECK IF EVERYTHING IS OKAY INSIDE THE MODULE!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      if(lsmpi_enabled_comm_procs)call lsquit("ERROR(lsmpi_finalize):&
      &comm processes were enabled on shutdown, this should never occur",-1)

#ifdef VAR_CHEMSHELL
      ! jump out of LSDALTON if a slave (instead of STOP)
      if (infpar%mynum.ne.infpar%master) call lsdaltonjumpout(99)
#else

      call MPI_FINALIZE(ierr)

      if(ierr/=0)then
        write (*,*) "mpi_finalize returned",ierr
        call LSMPI_MYFAIL(ierr)
        call lsquit("ERROR(MPI_FINALIZE):non zero exit)",-1)
      endif

#endif 
#endif 
    end subroutine lsmpi_finalize
 

  !> \biref Slave routine for initializing MPI groups.
  !> Get groupsize from master and calls main group initialization routine.
  !> \author Kasper Kristensen
  !> \date March 2012
  subroutine init_mpi_groups_slave
#ifdef VAR_MPI
    implicit none
    integer(kind=ls_mpik) :: groupsize
    integer :: lupri

    ! Set output to standard output (currently not used for slave anyway)
    lupri=6

    ! Get groupsize from master
    call ls_mpibcast(groupsize,infpar%master,MPI_COMM_LSDALTON)
    ! Main group initialization routine
    call init_mpi_groups(groupsize,lupri)

#endif

  end subroutine init_mpi_groups_slave

  subroutine lsmpi_comm_group(comm,group)
     implicit none
     integer(kind=ls_mpik), intent(in) :: comm
     integer(kind=ls_mpik), intent(inout) :: group
     integer(kind=ls_mpik) :: ierr
     ierr = 0_ls_mpik
#ifdef VAR_MPI
     call MPI_COMM_GROUP(comm, group, ierr) 
#endif
  end subroutine lsmpi_comm_group

  subroutine lsmpi_group_free(group)
     implicit none
     integer(kind=ls_mpik), intent(inout) :: group
     integer(kind=ls_mpik) :: ierr
     ierr = 0_ls_mpik
#ifdef VAR_MPI
     call MPI_GROUP_FREE(group, ierr) 
#endif
  end subroutine lsmpi_group_free

  subroutine lsmpi_iprobe(comm,MessageRecieved,lsmpi_status)
    implicit none
    integer(kind=ls_mpik), intent(in) :: comm
#ifdef VAR_MPI
    integer(kind=ls_mpik), intent(inout) :: lsmpi_status(MPI_STATUS_SIZE) 
#else
    integer(kind=ls_mpik), intent(inout) :: lsmpi_status(:) 
#endif
    logical,intent(inout) :: MessageRecieved
    !local variable
    logical(kind=ls_mpik) :: flag
    integer(kind=ls_mpik) :: IERR
#ifdef VAR_MPI
    call MPI_IPROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,comm,flag,lsmpi_status,ierr)
#else
    flag = .false.
#endif
    MessageRecieved = flag
  end subroutine lsmpi_iprobe

  subroutine lsmpi_probe(comm,lsmpi_status)
    implicit none
    integer(kind=ls_mpik), intent(in) :: comm
#ifdef VAR_MPI
    integer(kind=ls_mpik), intent(inout) :: lsmpi_status(MPI_STATUS_SIZE) 
#else
    integer(kind=ls_mpik), intent(inout) :: lsmpi_status(:) 
#endif
    integer(kind=ls_mpik) :: IERR
#ifdef VAR_MPI
    call MPI_PROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,comm,lsmpi_status,ierr)
#endif
  end subroutine lsmpi_probe

end module lsmpi_type


