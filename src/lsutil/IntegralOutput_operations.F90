!> @file 
!> Contains OBJECT CONTAINING INFORMATION ABOUT THE INTEGRAL OUTPUT
MODULE integraloutput_type
  use ThermiteTensor_type
  use integraloutput_typetype
  use precision
  use lstensor_typetype
  use memory_handling
  use Matrix_module, only: matrix

  private
  public :: nullifyIntegralOutput,initIntegralOutputDims,&
       & initIntegralOutputPDM,initIntegralOutputDims1,setIntegralOutput,&
       & init_IntOutOp, set_IntOutOp, IntOutOp_DetermineDimension

  INTERFACE setIntegralOutput
    MODULE PROCEDURE setIntegralOutputMat
  END INTERFACE setIntegralOutput

  integer :: IntOutOp_nreduced
  integer :: IntOutOp_nfull
  logical :: IntOutOp_LinearDepRemoved
  

CONTAINS
  subroutine init_IntOutOp()
    implicit none
    IntOutOp_nreduced = 0
    IntOutOp_nfull = 0
    IntOutOp_LinearDepRemoved = .FALSE.
  end subroutine init_IntOutOp

  subroutine set_IntOutOp(N,M)
    implicit none
    integer,intent(in) :: N,M
    IntOutOp_nreduced = M
    IntOutOp_nfull = N
    IntOutOp_LinearDepRemoved = .TRUE.
  end subroutine set_IntOutOp
  
SUBROUTINE setIntegralOutputMat(IntOut,M)
implicit none
TYPE(INTEGRALOUTPUT) :: IntOut
TYPE(MATRIX)         :: M

END SUBROUTINE setIntegralOutputMat

SUBROUTINE nullifyIntegralOutput(IntOut)
implicit none
TYPE(INTEGRALOUTPUT) :: IntOut
NULLIFY(IntOut%resultTensor)
NULLIFY(IntOut%ResultMat)
NULLIFY(IntOut%Result3D)
NULLIFY(IntOut%screenTensor)
NULLIFY(IntOut%postprocess)
NULLIFY(IntOut%IBUF)
NULLIFY(IntOut%RBUF)
NULLIFY(IntOut%NBUF)
IntOut%decpacked = .FALSE.
IntOut%decpacked2 = .FALSE.
IntOut%decpackedK = .FALSE.
IntOut%FullAlphaCD = .FALSE.
IntOut%exchangefactor = 0E0_realk
IntOut%ndim(1) = 0
IntOut%ndim(2) = 0
IntOut%ndim(3) = 0
IntOut%ndim(4) = 0
IntOut%ndim(5) = 0
IntOut%ndim3D(1) = 0
IntOut%ndim3D(2) = 0
IntOut%ndim3D(3) = 0
IntOut%memdistResultTensor = .FALSE.
IntOut%doGRAD = .FALSE.
IntOut%RealGabMatrix = .FALSE.
IntOut%MemDistMatrix = .FALSE.
IntOut%MemdistInt(1)=0
IntOut%MemdistInt(2)=0
IntOut%MemdistInt(3)=0
IntOut%MemdistInt(4)=0
END SUBROUTINE nullifyIntegralOutput

!> \brief set the dimensions of the integral output structure
!> \author T. Kjaergaard
!> \date 2010
!> \param IntOut the integraloutput structure to be initialised
!> \param dim1 size og dimension 1
!> \param dim2 size og dimension 2
!> \param dim3 size og dimension 3
!> \param dim4 size og dimension 4
!> \param dim5 size og dimension 5
SUBROUTINE initIntegralOutputDims(IntOut,dim1,dim2,dim3,dim4,dim5)
implicit none
TYPE(INTEGRALOUTPUT) :: IntOut
INTEGER              :: dim1,dim2,dim3,dim4,dim5
!
INTEGER              :: dimA,dimB,dimC,dimD

NULLIFY(IntOut%resultTensor)
IntOut%decpacked = .FALSE.
IntOut%decpacked2 = .FALSE.
IntOut%decpackedK = .FALSE.
IntOut%FullAlphaCD = .FALSE.
call mem_alloc(IntOut%postprocess,dim5)
IntOut%postprocess = 0
IF(IntOutOp_LinearDepRemoved)THEN
   dimA = IntOutOp_DetermineDimension(dim1)
   dimB = IntOutOp_DetermineDimension(dim2)
   dimC = IntOutOp_DetermineDimension(dim3)
   dimD = IntOutOp_DetermineDimension(dim4)
   call initIntegralOutputDims1(IntOut,dimA,dimB,dimC,dimD,dim5)
ELSE
   call initIntegralOutputDims1(IntOut,dim1,dim2,dim3,dim4,dim5)
ENDIF
IntOut%RealGabMatrix = .FALSE.
IntOut%MemDistMatrix = .FALSE.
END SUBROUTINE initIntegralOutputDims

integer function IntOutOp_DetermineDimension(dim1)
  implicit none
  integer,intent(in) :: dim1
  
  IF(IntOutOp_LinearDepRemoved)THEN
     IF(dim1.EQ.IntOutOp_nreduced)THEN
        IntOutOp_DetermineDimension = IntOutOp_nfull
     ELSE
        IntOutOp_DetermineDimension = dim1
     ENDIF
  ELSE
     IntOutOp_DetermineDimension = dim1
  ENDIF
end function IntOutOp_DetermineDimension

SUBROUTINE initIntegralOutputPDM(memdist,IntOut,dim1,dim2,i,j)
implicit none
TYPE(INTEGRALOUTPUT),intent(inout) :: IntOut
INTEGER,intent(in)   :: dim1,dim2,i,j
LOGICAL,intent(in)   :: memdist
#ifdef VAR_SCALAPACK
IF(memdist)THEN
   IntOut%MemDistMatrix = .TRUE.
   IntOut%MemdistInt(1)=dim1
   IntOut%MemdistInt(2)=dim2
   IntOut%MemdistInt(3)=i
   IntOut%MemdistInt(4)=j
ENDIF
#endif
END SUBROUTINE initIntegralOutputPDM

!> \brief set the dimensions of the integral output structure
!> \author T. Kjaergaard
!> \date 2010
!> \param IntOut the integraloutput structure to be initialised
!> \param dim1 size og dimension 1
!> \param dim2 size og dimension 2
!> \param dim3 size og dimension 3
!> \param dim4 size og dimension 4
!> \param dim5 size og dimension 5
SUBROUTINE initIntegralOutputDims1(IntOut,dim1,dim2,dim3,dim4,dim5)
implicit none
TYPE(INTEGRALOUTPUT) :: IntOut
INTEGER              :: dim1,dim2,dim3,dim4,dim5

IntOut%decpacked = .FALSE.
IntOut%decpacked2 = .FALSE.
IntOut%decpackedK = .FALSE.
IntOut%FullAlphaCD = .FALSE.
IntOut%exchangefactor = 0E0_realk
IntOut%ndim(1) = dim1
IntOut%ndim(2) = dim2
IntOut%ndim(3) = dim3
IntOut%ndim(4) = dim4
IntOut%ndim(5) = dim5
IntOut%memdistResultTensor = .FALSE.
END SUBROUTINE initIntegralOutputDims1

end MODULE integraloutput_type
