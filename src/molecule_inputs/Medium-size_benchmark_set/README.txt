Refer to Simen Reine (simen.reine@kjemi.uio.no) for the origin of those molecule inputs.


    1. references where presented:

    2. references where used:
       Future article on the "PARI approximation (Pair-Atomic Resolution-of-the-Identity)"

    3. list of atoms it contains:

    4. size of molecules involved:

    5. closed shell molecules only are present in this benchmark sets

    6. properties it has been benchmarked for:

    7. Which basis set it has been used for:
       6-31G/df-def2,
       6-31G*/df-def2,
       cc-pVTZ/cc-pVTZdenfit,
       cc-pVTZ/cc-pVTZ-RI

