FROM ubuntu:16.04

MAINTAINER Roberto Di Remigio  <roberto.d.remigio@uit.no>

RUN apt -y update; \
    apt -y upgrade; \
    apt -y install git g++ gcc gfortran cmake libopenmpi-dev mpi-default-bin wget bzip2

SHELL ["/bin/bash", "-c"]

RUN adduser --disabled-password --gecos '' --system --shell /bin/bash mightybuilder
RUN adduser mightybuilder sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER mightybuilder
ENV HOME /home/mightybuilder

WORKDIR $HOME

RUN wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O miniconda.sh; \
    bash miniconda.sh -b -p $HOME/Software/miniconda
ENV PATH $HOME/Software/miniconda/bin:$PATH
RUN conda config --set always_yes yes --set changeps1 no; \
    conda update --quiet conda; \
    conda info --all; \
    conda create --quiet -n lsdalton python=2.7 mkl mkl-static mkl-devel numpy matplotlib -c intel; \
    conda list
