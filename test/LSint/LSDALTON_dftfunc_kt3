#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_dftfunc_kt3.info <<'%EOF%'
   LSDALTON_dftfunc_kt3
   -------------
   Molecule:         water/6-31G**
   Wave Function:    KT3
   Test Purpose:     Check DFT-functional energy
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_dftfunc_kt3.mol <<'%EOF%'
BASIS
6-31G**
LSint test, H2O
6-31G**
Atomtypes=2 Nosymmetry
Charge=8.0 Atoms=1
O     0.000000000  -0.224905893   0.00000000
Charge=1.0 Atoms=2
H     1.452350000   0.899623000   0.00000000
H    -1.452350000   0.899623000   0.00000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_dftfunc_kt3.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
 KT3
*DFT INPUT
.MEMORY
.GRID TYPE
 SSF LMG
.RADINT
1.0D-11
.ANGINT
31
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_dftfunc_kt3.check
cat >> LSDALTON_dftfunc_kt3.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Final DFT energy:  * \-77\.386719" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
