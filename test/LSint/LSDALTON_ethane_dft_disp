#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_ethane_dft_disp.info <<'%EOF%'
   Ethane D-BLYP
   -------------
   Molecule:         Ethane
   Wave Function:    D-BLYP / 6-31G
   Test Purpose:     Test energy and gradient contributions
                     with empirical DFT-D3 dispersion correction
                     with Becke-Johnson damping
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_ethane_dft_disp.mol <<'%EOF%'
BASIS
6-31G
D-BLYP energy and gradient
Ethane 
    2   0
        6.    2
C      1.4496747688     -0.0000010117     0.0000000000
C     -1.4496747688      0.0000010117     0.0000000000
        1.    6
H      2.1888645654     -0.9728762646     1.6850699397
H      2.1888645654     -0.9728762646    -1.6850699397
H     -2.1888645654      0.9728762646     1.6850699397
H     -2.1888645654      0.9728762646    -1.6850699397
H      2.1888617992      1.9457507963     0.0000000000
H     -2.1888617992     -1.9457507963     0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_ethane_dft_disp.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
 BLYP
*DFT INPUT
.DISPER
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
.CONVDYN
VTIGHT
**INFO
.DEBUG_MPI_MEM
**RESPONS
*MOLGRA
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSDALTON_ethane_dft_disp.check
cat >> LSDALTON_ethane_dft_disp.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "E_disp\: * \-0\.006724333" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=11
ERROR[1]="DISPERSION CORRECTION NOT CORRECT -"

CRIT2=`$GREP "Final * DFT energy\: * \-79\.75457103" $log | wc -l`
TEST[2]=`expr   $CRIT2`
CTRL[2]=1
ERROR[2]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT3=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT3`
CTRL[3]=1
ERROR[3]="Memory leak -"

# DISP FACTORS
CRIT4=`$GREP "s_6 =       1.000000000000     sr_6 =       0.429800000000" $log | wc -l`
TEST[4]=`expr  $CRIT4`
CTRL[4]=19
ERROR[4]="s_6 or sr_6 factor wrong -"

# DISP FACTORS
CRIT5=`$GREP "s_8 =       2.699600000000     sr_8 =       4.235900000000" $log | wc -l`
TEST[5]=`expr  $CRIT5`
CTRL[5]=19
ERROR[5]="s_8 or sr_8 wrong -"

# DISP FACTORS
CRIT6=`$GREP "alp =      14.000000000000" $log | wc -l`
TEST[6]=`expr  $CRIT6`
CTRL[6]=19
ERROR[6]="s_8 or sr_8 wrong -"

# Gradient tests
CRIT7=`$GREP "C * \-0\.0663511[0-9][0-9][0-9] * \-0\.00000964[7-9][0-9] * [- ]0\.0000000000" $log | wc -l`
TEST[7]=`expr  $CRIT7`
CTRL[7]=1
ERROR[7]="Error in XC gradient"

# Gradient tests
CRIT8=`$GREP "H * 0\.07412405[0-9][0-9] * \-0\.09893163[0-9][0-9] *  0\.17135526[0-9][0-9]" $log | wc -l` 
TEST[8]=`expr  $CRIT8`
CTRL[8]=1
ERROR[8]="Error in XC gradient"

# Gradient tests
CRIT9=`$GREP "H * 0\.07412326[0-9][0-9] * 0\.19786281[0-9][0-9] * [- ]0\.0000000000" $log | wc -l`
TEST[9]=`expr  $CRIT9`
CTRL[9]=1
ERROR[9]="Error in XC gradient"

# Gradient tests
CRIT10=`$GREP "C * \-0\.00221789[0-9][0-9] * \-0\.00000859[0-9][0-9] * [- ]0\.0000000000" $log | wc -l` 
TEST[10]=`expr  $CRIT10`
CTRL[10]=1
ERROR[10]="Error in molecular gradient"

# Gradient tests
CRIT11=`$GREP "H * \-0\.00196579[0-9][0-9] * 0\.00044165[0-9][0-9] *  \-0\.00076486[0-9][0-9]" $log | wc -l`
TEST[11]=`expr  $CRIT11`
CTRL[11]=1
ERROR[11]="Error in molecular gradient"

# Gradient tests
CRIT12=`$GREP "H * \-0\.00196571[0-9][0-9] * \-0\.00088368[0-9][0-9] * [- ]0\.0000000000" $log | wc -l`
TEST[12]=`expr  $CRIT12`
CTRL[12]=1
ERROR[12]="Error in molecular gradient"

# MPI Memory test
CRIT13=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[13]=`expr  $CRIT13`
CTRL[13]=0
ERROR[13]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5 6 7 9 10 11 12 13 
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
######################################################################
