     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | pablo
     Host                     | pablo-AU
     System                   | Linux-3.13.0-37-generic
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     BLAS                     | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_gf_lp64.so;/opt/intel/composer_xe_2015.2.16
                              | 4/mkl/lib/intel64/libmkl_sequential.so;/opt/intel/
                              | composer_xe_2015.2.164/mkl/lib/intel64/libmkl_core
                              | .so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/l
                              | ib/x86_64-linux-gnu/libm.so
     LAPACK                   | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_lapack95_lp64.a;/opt/intel/composer_xe_2015
                              | .2.164/mkl/lib/intel64/libmkl_gf_lp64.so
     Static linking           | OFF
     Last Git revision        | 8bbbed1ef6db7a4a1c1ae67178f9daf76febb265
     Git branch               | pablo/lofex
     Configuration time       | 2016-09-30 11:36:02.317236
  

         Start simulation
     Date and time (Linux)  : Fri Sep 30 12:11:06 2016
     Host name              : pablo-AU                                
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G                                   
                                            
                                            
    Atomtypes=2 Nosymmetry Angstrom Charge=0                                                                                
    Charge=8 Atoms=1                                                                                                        
    O   -2.904830    1.255230    0.000000                                                                                   
    Charge=1 Atoms=2                                                                                                        
    H   -1.934830    1.255230    0.000000                                                                                   
    H   -3.228160    0.442870   -0.420050                                                                                   
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .RH
    .DIIS
    .START
    ATOMS
    .LCM
    .MAXIT
    1000
    **CC
    .CCS
    .CANONICAL
    .MEMORY
    1.0
    *CCRESPONSE
    .NEXCIT
    3
    .SPECTRUM
    .OPERATOR
     DIPVEL
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
          2 H      1.000 6-31G                      4        2 [4s|2s]                                      
          3 H      1.000 6-31G                      4        2 [4s|2s]                                      
    ---------------------------------------------------------------------
    total         10                               30       13
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       13
      Primitive Regular basisfunctions   :       30
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    Density subspace min. method    : DIIS                    
    Density optimization            : Diagonalization                    

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Due to the presence of the keyword (default for correlation)
    .NOGCINTEGRALTRANSFORM
    We transform the input basis to the Grand Canonical
    basis and perform integral evaluation using this basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on 6-31G Charge   8
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -71.4946859143    0.00000000000    0.00      0.00000    0.00    0.0000000    4.531E+00 ###
      2    -73.8341599123   -2.33947399805    0.00      0.00000    0.00    0.0000000    2.663E+00 ###
      3    -74.2580432558   -0.42388334348   -1.00      0.00000    0.00    0.0000000    1.607E-01 ###
      4    -74.2598387062   -0.00179545041   -1.00      0.00000    0.00    0.0000000    3.161E-02 ###
      5    -74.2598920070   -0.00005330082   -1.00      0.00000    0.00    0.0000000    2.600E-03 ###
      6    -74.2598924085   -0.00000040150   -1.00      0.00000    0.00    0.0000000    1.479E-06 ###

    Level 1 atomic calculation on 6-31G Charge   1
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -0.3410549973    0.00000000000    0.00      0.00000    0.00    0.0000000    1.982E-01 ###
      2     -0.3488197504   -0.00776475307    0.00      0.00000    0.00    0.0000000    1.270E-02 ###
      3     -0.3488518065   -0.00003205613   -1.00      0.00000    0.00    0.0000000    3.937E-05 ###
 
    Matrix type: mtype_dense

    First density: Atoms in molecule guess

    Iteration 0 energy:      -75.944138361131
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -75.9265805613    0.00000000000    0.00      0.00000    0.00    0.0000000    1.718E+00 ###
      2    -75.9685132390   -0.04193267771    0.00      0.00000    0.00    0.0000000    1.001E+00 ###
      3    -75.9843275074   -0.01581426836   -1.00      0.00000    0.00    0.0000000    6.887E-02 ###
      4    -75.9845163177   -0.00018881031   -1.00      0.00000    0.00    0.0000000    1.176E-02 ###
      5    -75.9845230822   -0.00000676444   -1.00      0.00000    0.00    0.0000000    1.694E-03 ###
      6    -75.9845231992   -0.00000011701   -1.00      0.00000    0.00    0.0000000    1.099E-04 ###
      7    -75.9845231996   -0.00000000046   -1.00      0.00000    0.00    0.0000000    1.980E-05 ###
    SCF converged in      7 iterations
    >>>  CPU Time used in SCF iterations is   0.38 seconds
    >>> wall Time used in SCF iterations is   0.38 seconds

    Total no. of matmuls in SCF optimization:         59

    Number of occupied orbitals:       5
    Number of virtual orbitals:        8

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     5 iterations!

    Calculation of virtual orbital energies converged in     6 iterations!

     E(LUMO):                         0.201605 au
    -E(HOMO):                        -0.498125 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.699730 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.04193267771    0.0000    0.0000    0.0000000
      3   -0.01581426836   -1.0000    0.0000    0.0000000
      4   -0.00018881031   -1.0000    0.0000    0.0000000
      5   -0.00000676444   -1.0000    0.0000    0.0000000
      6   -0.00000011701   -1.0000    0.0000    0.0000000
      7   -0.00000000046   -1.0000    0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1           -75.92658056132371768854      0.171788818626525D+01
        2           -75.96851323903459274334      0.100068010643628D+01
        3           -75.98432750739429764053      0.688657971245424D-01
        4           -75.98451631770838332613      0.117552000494058D-01
        5           -75.98452308215087214194      0.169385283069236D-02
        6           -75.98452319916466990435      0.109915107778845D-03
        7           -75.98452319962056833447      0.198017815317274D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                       -75.984523199621
          Nuclear repulsion:                       9.062741299348
          Electronic energy:                     -85.047264498969

      %LOC%
      %LOC% Localized orbitals written to lcm_orbitals.u
      %LOC%

    Number of electrons  in molecule =       10
    Number of occ. orb.  in molecule =        5
    Number of virt. orb. in molecule =        8




    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :     10
    FULL: Number of atoms               :      3
    FULL: Number of basis func.         :     13
    FULL: Number of aux. basis func.    :      0
    FULL: Number of core orbitals       :      1
    FULL: Number of valence orbitals    :      4
    FULL: Number of occ. orbitals       :      5
    FULL: Number of occ. alpha orbitals :      0
    FULL: Number of occ. beta  orbitals :      0
    FULL: Number of virt. orbitals      :      8
    FULL: Local memory use type full    :  0.32E-05
    FULL: Distribute matrices           : F
    FULL: Using frozen-core approx.     : F

 Allocate space for molecule%Co on Master use_bg= F
 Allocate space for molecule%Cv on Master use_bg= F
    Memory set in input to be:    1.000     GB


    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================

    Using canonical orbitals as requested in input!



 ================================================ 
              Full molecular driver               
 ================================================ 



 JAC ********************************************************
 JAC        Information for Jacobian eigenvalue solver       
 JAC        ------------------------------------------       
 JAC 
 JAC Number of eigenvalues                 3
 JAC Initial subspace dimension            3
 JAC Maximum subspace dimension           40
 JAC 
 JAC Start guess for eigenvalues
 JAC ---------------------------
 JAC       1    0.6997303087    
 JAC       2    0.7508053349    
 JAC       3    0.7954661590    
 JAC 
 JAC ********************************************************
 JAC
 JAC Jacobian eigenvalue solver
 JAC
 JAC Which   Subspace     Eigenvalue           Residual       Conv?  
 JAC     1       3        0.35469444         0.11595923        F
 JAC     2       4        0.43568718         0.13942446        F
 JAC     3       5        0.44211849         0.15255397        F
 JAC     1       6        0.34045517         0.11490908E-01    F
 JAC     2       7        0.41202592         0.71682942E-03    F
 JAC     3       8        0.42336299         0.45500604E-01    F
 JAC     1       9        0.34033748         0.16088013E-02    F
 JAC     2      10        0.41202538         0.37549197E-06    T
 JAC     3      10        0.42146864         0.73926635E-02    F
 JAC     1      11        0.34033688         0.36743512E-03    F
 JAC     2      12        0.41202538         0.37562282E-06    T
 JAC     3      12        0.42139270         0.22005080E-02    F
 JAC     1      13        0.34033687         0.71279374E-05    T
 JAC     2      13        0.41202538         0.37476577E-06    T
 JAC     3      13        0.42139079         0.85644345E-03    F
 JAC     1      14        0.34033687         0.71279374E-05    T
 JAC     2      14        0.41202538         0.37476577E-06    T
 JAC     3      14        0.42139071         0.90698182E-04    T


      ************************************************************
      *               CCS      excitation energies               *
      ************************************************************

            Exci.    Hartree           eV            cm-1         
              1      0.3403369       9.261042       74695.31    
              2      0.4120254       11.21179       90429.12    
              3      0.4213907       11.46663       92484.57    

------------------------------------------
  Coupled-cluster transition multipliers  
------------------------------------------

Wave function    = CCS     
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting Lagrangian iterations
 ### ------------------------------
 ###  Iteration     Residual norm
 ###      1         0.136421381E-01
 ###      2         0.190273297E-02
 ###      3         0.931257508E-04
 ###      4         0.430506998E-05
 ###      5         0.340037943E-06
 ###      6         0.234407479E-08
 ###      7         0.483690084E-11
 CCSOL: MAIN LOOP      :  1.56    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
CCSOL: Total cpu time    =  0.256     s
CCSOL: Total wall time   =  0.257     s
Singles multiplier norm  =  0.8075482E-01
Total multiplier norm    =  0.8075482E-01
Number of CC iterations  =    7

------------------------------------------
  Coupled-cluster transition multipliers  
------------------------------------------

Wave function    = CCS     
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting Lagrangian iterations
 ### ------------------------------
 ###  Iteration     Residual norm
 ###      1         0.938827752E-02
 ###      2         0.894740166E-03
 ###      3         0.842205096E-04
 ###      4         0.604104483E-05
 ###      5         0.997166507E-09
 CCSOL: MAIN LOOP      :  1.78    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
CCSOL: Total cpu time    =  0.184     s
CCSOL: Total wall time   =  0.182     s
Singles multiplier norm  =  0.8129322E-01
Total multiplier norm    =  0.8129322E-01
Number of CC iterations  =    5

------------------------------------------
  Coupled-cluster transition multipliers  
------------------------------------------

Wave function    = CCS     
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting Lagrangian iterations
 ### ------------------------------
 ###  Iteration     Residual norm
 ###      1         0.478883813E-01
 ###      2         0.196101324E-01
 ###      3         0.107707191E-03
 ###      4         0.584283794E-05
 ###      5         0.409173081E-06
 ###      6         0.168715167E-07
 ###      7         0.681302113E-09
 CCSOL: MAIN LOOP      :  2.23    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
CCSOL: Total cpu time    =  0.388     s
CCSOL: Total wall time   =  0.392     s
Singles multiplier norm  =  0.7865092E-01
Total multiplier norm    =  0.7865092E-01
Number of CC iterations  =    7

 CCS transition moments module:
 ------------------------------

      xi  density matrix:       0.00 min.
      eta density matrix:       0.00 min.
      total wall time   :       0.00 min.


 Velocity gauge was used for the dipole moments integrals


 CCS      Right transition dipole moments (a.u.):
 ------------------------------------------------

 State            X               Y               Z             Freq.
    1        0.00000000     -0.04732122      0.09151737      0.34033687
    2       -0.00000000      0.00000133     -0.00000257      0.41202538
    3        0.11716688     -0.14719107     -0.07610863      0.42139071


 CCS      Left  transition dipole moments (a.u.):
 ------------------------------------------------

 State            X               Y               Z             Freq.
    1       -0.00000000      0.09423817     -0.18225288      0.34033687
    2        0.00000000     -0.00000267      0.00000516      0.41202538
    3       -0.24414832      0.30671242      0.15859293      0.42139071


 CCS      transition dipole strengths (a.u.):
 --------------------------------------------

 State            X               Y               Z             Freq.
    1       -0.00000000     -0.00445947     -0.01667930      0.34033687
    2       -0.00000000     -0.00000000     -0.00000000      0.41202538
    3       -0.02860610     -0.04514533     -0.01207029      0.42139071


 CCS      Oscillator strengths (Velocity gauge):
 ===============================================

 State        Osc. str.     Freq. (a.u.)         (eV)          (cm-1) 
    1       -0.04140754      0.34033687      9.26104237      74695.3067
    2       -0.00000000      0.41202538     11.21178713      90429.1165
    3       -0.13577537      0.42139071     11.46663090      92484.5690






    ******************************************************************************
    *                      Full CCS      calculation is done !                   *
    ******************************************************************************





    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************

     E: Hartree-Fock energy                            :      -75.9845231996
     E: Correlation energy                             :        0.0000000000
     E: Total CCS energy                               :      -75.9845231996



    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in CC           :         1.60800     s
    Total Wall time used in CC           :         1.61600     s
    ------------------------------------------------------


    Hostname       : pablo-AU                                          
    Job finished   : Date: 30/09/2016   Time: 12:11:08



    =============================================================================
                              -- end of CC program --
    =============================================================================



    Total no. of matmuls used:                        96
    Total no. of Fock/KS matrix evaluations:           8
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          6.719 MB
      Max allocated memory, type(matrix)                  46.024 kB
      Max allocated memory, real(realk)                    6.616 MB
      Max allocated memory, integer                      103.508 kB
      Max allocated memory, logical                        1.092 kB
      Max allocated memory, character                      2.640 kB
      Max allocated memory, AOBATCH                       19.968 kB
      Max allocated memory, ARRAY                          4.992 kB
      Max allocated memory, ODBATCH                        2.640 kB
      Max allocated memory, LSAOTENSOR                     5.472 kB
      Max allocated memory, SLSAOTENSOR                    5.520 kB
      Max allocated memory, ATOMTYPEITEM                 153.056 kB
      Max allocated memory, ATOMITEM                       2.048 kB
      Max allocated memory, LSMATRIX                       4.032 kB
      Max allocated memory, OverlapT                      40.896 kB
      Max allocated memory, linkshell                      0.432 kB
      Max allocated memory, integrand                    716.800 kB
      Max allocated memory, integralitem                   1.280 MB
      Max allocated memory, IntWork                      161.608 kB
      Max allocated memory, Overlap                        4.308 MB
      Max allocated memory, ODitem                         1.920 kB
      Max allocated memory, LStensor                       9.887 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   2.23 seconds
    >>> wall Time used in LSDALTON is   2.24 seconds

    End simulation
     Date and time (Linux)  : Fri Sep 30 12:11:08 2016
     Host name              : pablo-AU                                
